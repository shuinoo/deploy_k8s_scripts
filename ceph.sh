#!/bin/bash 

# 生成 ceph yum 源
echo "生成 ceph yum 源"
cd /root/.k8s
cat > ceph.repo << EOF
[Ceph]
name=Ceph packages for \$basearch
baseurl=ftp://ftp-server-usa.joyslink.com/repos/ceph/rpm-nautilus/el7/\$basearch
enabled=1
gpgcheck=1
type=rpm-md
gpgkey=ftp://ftp-server-usa.joyslink.com/repos/ceph/release.asc
priority=1

[Ceph-noarch]
name=Ceph noarch packages
baseurl=ftp://ftp-server-usa.joyslink.com/repos/ceph/rpm-nautilus/el7/noarch
enabled=1
gpgcheck=1
type=rpm-md
gpgkey=ftp://ftp-server-usa.joyslink.com/repos/ceph/release.asc
priority=1

[ceph-source]
name=Ceph source packages
baseurl=ftp://ftp-server-usa.joyslink.com/repos/ceph/rpm-nautilus/el7/SRPMS
enabled=1
gpgcheck=1
type=rpm-md
gpgkey=ftp://ftp-server-usa.joyslink.com/repos/ceph/release.asc
EOF

# 分发 ceph yum 源到所有主机
echo "分发 ceph yum 源到所有主机"
cd /root/.k8s
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp ceph.repo root@${node_ip}:/etc/yum.repos.d/
done

# 安装使用 ceph 存储必要的依赖包
echo "安装使用 ceph 存储必要的依赖包"
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "yum install -y ceph-common"
done

# 生成rbd 动态供给 yaml 文件
echo "生成 rbd 动态供给 yaml 文件"
cd /root/.k8s/storage/ceph/rbd
cat > ceph-rbd.yaml << EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
  name: rbd-provisioner
  namespace: kube-system
spec:
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: rbd-provisioner
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: rbd-provisioner
    spec:
      containers:
      - env:
        - name: PROVISIONER_NAME
          value: ceph.com/rbd
        image: quay.io/external_storage/rbd-provisioner:latest
        imagePullPolicy: Always
        name: rbd-provisioner
      restartPolicy: Always
      securityContext: {}
      serviceAccount: rbd-provisioner
      serviceAccountName: rbd-provisioner
      terminationGracePeriodSeconds: 30
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: rbd-provisioner
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: rbd-provisioner
rules:
- apiGroups:
  - ""
  resources:
  - persistentvolumes
  verbs:
  - get
  - list
  - watch
  - create
  - delete
- apiGroups:
  - ""
  resources:
  - persistentvolumeclaims
  verbs:
  - get
  - list
  - watch
  - update
- apiGroups:
  - storage.k8s.io
  resources:
  - storageclasses
  verbs:
  - get
  - list
  - watch
- apiGroups:
  - ""
  resources:
  - events
  verbs:
  - create
  - update
  - patch
- apiGroups:
  - ""
  resources:
  - endpoints
  verbs:
  - get
  - list
  - watch
  - create
  - update
  - patch
- apiGroups:
  - ""
  resourceNames:
  - kube-dns
  - coredns
  resources:
  - services
  verbs:
  - list
  - get

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: rbd-provisioner
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: rbd-provisioner
subjects:
- kind: ServiceAccount
  name: rbd-provisioner
  namespace: kube-system

---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: rbd-provisioner
  namespace: kube-system
rules:
- apiGroups:
  - ""
  resources:
  - secrets
  verbs:
  - get
  - create
  - delete
- apiGroups:
  - ""
  resources:
  - endpoints
  verbs:
  - get
  - list
  - watch
  - create
  - update
  - patch

---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: rbd-provisioner
  namespace: kube-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: rbd-provisioner
subjects:
- kind: ServiceAccount
  name: rbd-provisioner
  namespace: kube-system
---
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
#  annotations:
#    storageclass.beta.kubernetes.io/is-default-class: "true"
  name: ceph-rbd
parameters:
  adminId: admin
  adminSecretName: ceph-admin-secret
  adminSecretNamespace: kube-system
  fsType: ext4
  imageFeatures: layering
  imageFormat: "2"
  monitors: 192.168.188.6:6789,192.168.188.7:6789,192.168.188.8:6789
  pool: kubernetes
  userId: kube
  userSecretName: ceph-secret
  userSecretNamespace: default
provisioner: ceph.com/rbd
reclaimPolicy: Delete
volumeBindingMode: Immediate
EOF
kubectl apply -f ceph-rbd.yaml

# 创建 admin sercets
cd /root/.k8s/storage/ceph/rbd
kubectl apply -f ceph-admin-kube-system-secret.yaml

# 在需要使用 ceph rbd 动态供给的命名空间创建 pvc 用于访问 ceph 的 secret
echo "在需要使用 ceph rbd 动态供给的命名空间创建 pvc 用于访问 ceph 的 secret"
kubectl apply -f ceph-default-secret.yaml
kubectl apply -f ceph-kube-system-secret.yaml

# 部署 cephfs-provisioner
echo "部署 cephfs-provisioner"
cd /root/.k8s/storage/ceph/cephfs
cat > cephfs.yaml << EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: cephfs-provisioner
  namespace: kube-system
spec:
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: cephfs-provisioner
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: cephfs-provisioner
    spec:
      containers:
      - args:
        - -id=cephfs-provisioner-1
        command:
        - /usr/local/bin/cephfs-provisioner
        env:
        - name: PROVISIONER_NAME
          value: ceph.com/cephfs
        - name: PROVISIONER_SECRET_NAMESPACE
          value: kube-system
        image: quay.io/external_storage/cephfs-provisioner:latest
        imagePullPolicy: Always
        name: cephfs-provisioner
        resources: {}
      restartPolicy: Always
      securityContext: {}
      serviceAccount: cephfs-provisioner
      serviceAccountName: cephfs-provisioner
      terminationGracePeriodSeconds: 30
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: cephfs-provisioner
  namespace: kube-system

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  annotations:
  name: cephfs-provisioner
rules:
- apiGroups:
  - ""
  resources:
  - persistentvolumes
  verbs:
  - get
  - list
  - watch
  - create
  - delete
- apiGroups:
  - ""
  resources:
  - persistentvolumeclaims
  verbs:
  - get
  - list
  - watch
  - create
  - delete
- apiGroups:
  - storage.k8s.io
  resources:
  - storageclasses
  verbs:
  - get
  - list
  - watch
- apiGroups:
  - ""
  resources:
  - events
  verbs:
  - create
  - update
  - patch
- apiGroups:
  - ""
  resources:
  - endpoints
  verbs:
  - get
  - list
  - watch
  - create
  - update
  - patch
- apiGroups:
  - ""
  resourceNames:
  - kube-dns
  - coredns
  resources:
  - services
  verbs:
  - list
  - get

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: cephfs-provisioner
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cephfs-provisioner
subjects:
- kind: ServiceAccount
  name: cephfs-provisioner
  namespace: kube-system

---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: cephfs-provisioner
  namespace: kube-system
rules:
- apiGroups:
  - ""
  resources:
  - secrets
  verbs:
  - create
  - get
  - delete
- apiGroups:
  - ""
  resources:
  - endpoints
  verbs:
  - get
  - list
  - watch
  - create
  - update
  - patch

---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: cephfs-provisioner
  namespace: kube-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: cephfs-provisioner
subjects:
- kind: ServiceAccount
  name: cephfs-provisioner
---
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: cephfs-provisioner
parameters:
  adminId: admin
  adminSecretName: ceph-admin-secret
  adminSecretNamespace: kube-system
  claimRoot: /volumes/kubernetes
  monitors: 192.168.188.6:6789,192.168.188.7:6789,192.168.188.8:6789
provisioner: ceph.com/cephfs
reclaimPolicy: Delete
volumeBindingMode: Immediate
EOF
kubectl apply -f cephfs.yaml
