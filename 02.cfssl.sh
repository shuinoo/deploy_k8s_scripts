#!/bin/bash

# 下载 cfssl 二进制
echo "下载 cfssl 二进制"
source /opt/k8s/bin/env.sh
mkdir -p /opt/k8s/bin && cd /opt/k8s/bin
[ ! -f /opt/k8s/bin/cfssl ] && wget ${Download_Server}/linux/cfssl/cfssl_${CFSSL_VERSION}_linux_amd64
mv cfssl_${CFSSL_VERSION}_linux_amd64 /opt/k8s/bin/cfssl
[ ! -f /opt/k8s/bin/cfssljson ] && wget ${Download_Server}/linux/cfssl/cfssljson_${CFSSL_VERSION}_linux_amd64
mv cfssljson_${CFSSL_VERSION}_linux_amd64 /opt/k8s/bin/cfssljson
[! -f /opt/k8s/bin/cfssl-certinfo ]wget ${Download_Server}/linux/cfssl/cfssl-certinfo_${CFSSL_VERSION}_linux_amd64
mv cfssl-certinfo_${CFSSL_VERSION}_linux_amd64 /opt/k8s/bin/cfssl-certinfo
chmod +x /opt/k8s/bin/*
