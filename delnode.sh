#!/bin/bash

# 驱逐 $1 节点
echo "驱逐 $1 节点"
kubectl drain $1 --ignore-daemonsets  --delete-local-data --force

# 禁止将pod调度该 $1 节点
echo "禁止将pod调度该 $1 节点"
kubectl cordon $1 
