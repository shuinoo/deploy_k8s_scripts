#!/bin/bash

# 创建 kube-scheduler 证书签名请求
echo "创建 kube-scheduler 证书签名请求"
cd /root/.k8s/deploy
cat > kube-scheduler-csr.json << EOF
{
    "CN": "system:kube-scheduler",
    "hosts": [
        "127.0.0.1",
        "${MASTER_IPS[0]}",
        "${MASTER_IPS[1]}",
        "${MASTER_IPS[2]}"
    ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names":[
        {
            "C": "CN",
            "ST": "GuangDong",
            "L": "ShenZhen",
            "O": "system:kube-scheduler",
            "OU": "System"
        }
    ]
}
EOF

# 生成 kube-scheduler 证书和私钥
echo "生成 kube-scheduler 证书和私钥"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cfssl gencert -ca=/root/.k8s/deploy/ca.pem \
  -ca-key=/root/.k8s/deploy/ca-key.pem \
  -config=/root/.k8s/deploy/ca-config.json \
  -profile=kubernetes kube-scheduler-csr.json | cfssljson -bare kube-scheduler

# 将生成的 kube-scheduler 证书和私钥分发到 master 节点
echo "将生成的 kube-scheduler 证书和私钥分发到 master 节点"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp kube-scheduler*.pem root@${node_ip}:/etc/kubernetes/ssl/
done

# 创建 kube-scheduler kubeconfig
echo "创建 kube-scheduler kubeconfig"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
kubectl config set-cluster ${KUBE_CLUSTER} \
  --certificate-authority=/root/.k8s/deploy/ca.pem \
  --embed-certs=true \
  --server=${KUBE_APISERVER} \
  --kubeconfig=kube-scheduler.kubeconfig
kubectl config set-credentials system:kube-scheduler \
  --client-certificate=kube-scheduler.pem \
  --client-key=kube-scheduler-key.pem \
  --embed-certs=true \
  --kubeconfig=kube-scheduler.kubeconfig
kubectl config set-context system:kube-scheduler \
  --cluster=${KUBE_CLUSTER} \
  --user=system:kube-scheduler \
  --kubeconfig=kube-scheduler.kubeconfig
kubectl config use-context system:kube-scheduler --kubeconfig=kube-scheduler.kubeconfig

# 分发 kube-scheduler kubeconfig 到所有 master 节点
echo "分发 kube-scheduler kubeconfig 到所有 master 节点"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp kube-scheduler.kubeconfig root@${node_ip}:/etc/kubernetes/
done

# 创建 kube-scheduler 配置文件
echo "创建 kube-scheduler 配置文件"
cd /root/.k8s/deploy
cat > kube-scheduler.yaml.template <<EOF
apiVersion: kubescheduler.config.k8s.io/v1alpha1
kind: KubeSchedulerConfiguration
bindTimeoutSeconds: 600
clientConnection:
  burst: 200
  kubeconfig: "/etc/kubernetes/kube-scheduler.kubeconfig"
  qps: 100
enableContentionProfiling: false
enableProfiling: true
hardPodAffinitySymmetricWeight: 1
healthzBindAddress: 127.0.0.1:10251
leaderElection:
  leaderElect: true
metricsBindAddress: ##NODE_IP##:10251
EOF

# 生成每个节点的 kube-scheduler.yaml
echo "生成每个节点的 kube-scheduler.yaml"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for (( i=0; i < 3; i++ )); do
    sed -e "s/##NODE_NAME##/${NODE_NAMES[i]}/" -e "s/##NODE_IP##/${NODE_IPS[i]}/" kube-scheduler.yaml.template > kube-scheduler-${NODE_IPS[i]}.yaml
done

# 分发生成的 kube-scheduler.yaml 到每个节点
echo "分发生成的 kube-scheduler.yaml 到每个节点"
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp kube-scheduler-${node_ip}.yaml root@${node_ip}:/etc/kubernetes/kube-scheduler.yaml
done

# 生成 kube-scheduler systemd 模板文件
echo "生成 kube-scheduler systemd 模板文件"
cd /root/.k8s/deploy
cat > kube-scheduler.service.template << EOF
[Unit]
Description=Kubernetes Scheduler
Documentation=https://github.com/GoogleCloudPlatform/kubernetes

[Service]
WorkingDirectory=${K8S_DIR}/kube-scheduler
ExecStart=/opt/k8s/bin/kube-scheduler \\
    --authentication-kubeconfig=/etc/kubernetes/kube-scheduler.kubeconfig \\
    --authorization-kubeconfig=/etc/kubernetes/kube-scheduler.kubeconfig \\
    --bind-address=##NODE_IP## \\
    --client-ca-file=/etc/kubernetes/ssl/ca.pem \\
    --config=/etc/kubernetes/kube-scheduler.yaml \\
    --logtostderr=true \\
    --port=0  \\
    --requestheader-client-ca-file=/etc/kubernetes/ssl/ca.pem \\
    --requestheader-extra-headers-prefix="X-Remote-Extra-" \\
    --requestheader-group-headers=X-Remote-Group \\
    --requestheader-username-headers=X-Remote-User \\
    --secure-port=10259 \\
    --v=2
Restart=on-failure
RestartSec=5
StartLimitInterval=0

[Install]
WantedBy=multi-user.target
EOF

# 生成每个节点的 kube-scheduler systemd
echo "生成每个节点的 kube-scheduler systemd"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for (( i=0; i < 3; i++ )); do
    sed -e "s/##NODE_NAME##/${NODE_NAMES[i]}/" -e "s/##NODE_IP##/${NODE_IPS[i]}/" kube-scheduler.service.template > kube-scheduler-${NODE_IPS[i]}.service 
done

# 分发生成的 kube-scheduler systemd 到每个 master 节点
echo "分发生成的 kube-scheduler systemd 到每个 master 节点"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp kube-scheduler-${node_ip}.service root@${node_ip}:/etc/systemd/system/kube-scheduler.service
done

# 启动 kube-scheduler 
echo "启动 kube-scheduler"
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "mkdir -p ${K8S_DIR}/kube-scheduler"
    ssh root@${node_ip} "systemctl daemon-reload && systemctl enable kube-scheduler  && systemctl stop kube-scheduler && systemctl restart kube-scheduler"
done

# 检查 kube-scheduler 运行状态
echo "检查 kube-scheduler 运行状态"
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "systemctl status kube-scheduler | grep Active"
done

# 查看每个节点 kube-scheduler metrics 输出
echo "查看每个节点 kube-scheduler metrics 输出"
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "curl -s http://${node_ip}:10251/metrics | head"
done
