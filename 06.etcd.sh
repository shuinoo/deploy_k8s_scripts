#!/bin/bash

# 下载 etcd 二进制文件
echo "下载 etcd 二进制文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
[ ! -f /root/.k8s/deploy/etcd-${ETCD_VERSION}-linux-amd64.tar.gz ] && wget ${Download_Server}/linux/etcd/etcd-${ETCD_VERSION}-linux-amd64.tar.gz
tar xf etcd-${ETCD_VERSION}-linux-amd64.tar.gz

# 分发二进制文件到 etcd 节点
echo "分发二进制文件到 etcd 节点"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${ETCD_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "systemctl stop etcd"
    scp etcd-${ETCD_VERSION}-linux-amd64/etcd* root@${node_ip}:/opt/k8s/bin
    ssh root@${node_ip} "chmod +x /opt/k8s/bin/*"
done

# 创建 etcd 证书请求文件
echo "创建 etcd 证书请求文件"
cd /root/.k8s/deploy
cat > etcd-csr.json <<EOF
{
    "CN": "etcd",
    "hosts": [
        "127.0.0.1",
        "${ETCD_IPS[0]}",
        "${ETCD_IPS[1]}",
        "${ETCD_IPS[2]}"
    ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "GuangDong",
            "L": "ShenZhen",
            "O": "k8s",
            "OU": "System"
        }
    ]
}
EOF


# 生成 etcd 证书和私钥
echo "生成 etcd 证书和私钥"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cfssl gencert -ca=/root/.k8s/deploy/ca.pem \
    -ca-key=/root/.k8s/deploy/ca-key.pem \
    -config=/root/.k8s/deploy/ca-config.json \
    -profile=kubernetes etcd-csr.json | cfssljson -bare etcd

# 分发 etcd 证书和私钥到 etcd 节点
echo "分发 etcd 证书和私钥到 etcd 节点"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${ETCD_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "mkdir -p /etc/etcd/ssl"
    scp etcd*.pem root@${node_ip}:/etc/etcd/ssl/
done

# 创建 etcd 启动模板文件
echo "创建 etcd 启动模板文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > etcd.service-template << EOF
[Unit]
Description=Etcd Server
After=network.target
After=network-online.target
Wants=network-online.target
Documentation=https://github.com/coreos

[Service]
Type=notify
WorkingDirectory=${ETCD_DATA_DIR}
ExecStart=/opt/k8s/bin/etcd \\
          --advertise-client-urls=https://##NODE_IP##:2379 \\
          --auto-compaction-mode=periodic \\
          --auto-compaction-retention=1 \\
          --cert-file=/etc/etcd/ssl/etcd.pem \\
          --client-cert-auth \\
          --data-dir=${ETCD_DATA_DIR} \\
          --election-timeout=2000 \\
          --heartbeat-interval=250 \\
          --initial-advertise-peer-urls=https://##NODE_IP##:2380 \\
          --initial-cluster-token=etcd-cluster-0 \\
          --initial-cluster=${ETCD_NODES} \\
          --initial-cluster-state=new \\
          --key-file=/etc/etcd/ssl/etcd-key.pem \\
          --listen-peer-urls=https://##NODE_IP##:2380 \\
          --listen-client-urls=https://##NODE_IP##:2379 \\
          --listen-metrics-urls=http://##NODE_IP##:2222 \\
          --max-request-bytes=33554432 \\
          --name=##NODE_NAME## \\
          --peer-cert-file=/etc/etcd/ssl/etcd.pem \\
          --peer-key-file=/etc/etcd/ssl/etcd-key.pem \\
          --peer-trusted-ca-file=/etc/kubernetes/ssl/ca.pem \\
          --peer-client-cert-auth \\
          --quota-backend-bytes=6442450944 \\
          --trusted-ca-file=/etc/kubernetes/ssl/ca.pem \\
          --wal-dir=${ETCD_WAL_DIR}
Restart=on-failure
RestartSec=5
LimitNOFILE=65536
[Install]
WantedBy=multi-user.target
EOF

# 生成各个节点 etcd 启动文件
echo "生成各个节点 etcd 启动文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for (( i=0; i < 3; i++ )); do
    sed -e "s/##NODE_NAME##/${ETCD_NAMES[i]}/" -e "s/##NODE_IP##/${ETCD_IPS[i]}/" etcd.service-template > etcd-${ETCD_IPS[i]}.service 
done


# 分发生成的 etcd 启动文件到对应的服务器
echo "分发生成的 etcd 启动文件到对应的服务器"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${ETCD_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp etcd-${node_ip}.service root@${node_ip}:/etc/systemd/system/etcd.service
done

# 启动 etcd 服务
echo "启动 etcd 服务"
source /opt/k8s/bin/env.sh
for node_ip in ${ETCD_IPS[@]}; do
    echo ">>> ${node_ip}"
    #创建 etcd 数据目录
    ssh root@${node_ip} "mkdir -p ${ETCD_DATA_DIR} ${ETCD_WAL_DIR}"
    ssh root@${node_ip} "systemctl daemon-reload && systemctl enable etcd && systemctl restart etcd"
done

# 检查启动结果
echo "检查 etcd 启动结果"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${ETCD_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "systemctl status etcd|grep Active"
done

# 验证 etcd 集群状态
echo "验证 etcd 集群状态"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${ETCD_IPS[@]}; do
    echo ">>> ${node_ip}"
    ETCDCTL_API=3 /opt/k8s/bin/etcdctl \
    --endpoints=https://${node_ip}:2379 \
    --cacert=/etc/kubernetes/ssl/ca.pem \
    --cert=/etc/etcd/ssl/etcd.pem \
    --key=/etc/etcd/ssl/etcd-key.pem endpoint health
done

source /opt/k8s/bin/env.sh
ETCDCTL_API=3 /opt/k8s/bin/etcdctl \
  -w table --cacert=/etc/kubernetes/ssl/ca.pem \
  --cert=/etc/etcd/ssl/etcd.pem \
  --key=/etc/etcd/ssl/etcd-key.pem \
  --endpoints=${ETCD_ENDPOINTS} endpoint status
