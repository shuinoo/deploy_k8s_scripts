#!/bin/bash

# 下载 apiserver 二进制
echo "下载 apiserver 二进制文件"
source /opt/k8s/bin/env.sh
cd /root/.k8s/deploy
[ ! -f /root/.k8s/deploy/kubernetes-server-linux-amd64.tar.gz ] && wget ${Download_Server}/linux/kubernetes/${KUBE_VERSION}/kubernetes-server-linux-amd64.tar.gz
tar xf kubernetes-server-linux-amd64.tar.gz

# 分发二进制文件到各 master 节点
echo 分发二进制文件到各 master 节点
source /opt/k8s/bin/env.sh
cd /root/.k8s/deploy
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "systemctl stop kube-apiserver"
    scp kubernetes/server/bin/{apiextensions-apiserver,cloud-controller-manager,kubeadm,kube-apiserver,kube-controller-manager,kube-scheduler,mounter} root@${node_ip}:/opt/k8s/bin/
done

# 生成 kubernetes 证书请求文件
echo "生成 kubernetes 证书请求文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > kubernetes-csr.json <<EOF
{
  "CN": "kubernetes",
  "hosts": [
    "127.0.0.1",
    "${MASTER_IPS[0]}",
    "${MASTER_IPS[1]}",
    "${MASTER_IPS[2]}",
    "${LB_IP}",
    "${CLUSTER_KUBERNETES_SVC_IP}",
    "kubernetes",
    "kubernetes.default",
    "kubernetes.default.svc",
    "kubernetes.default.svc.cluster",
    "kubernetes.default.svc.${CLUSTER_DNS_DOMAIN}"
  ],
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "CN",
      "ST": "GuangDong",
      "L": "ShenZhen",
      "O": "k8s",
      "OU": "System"
    }
  ]
}
EOF

# 生成 kubernetes 证书和私钥
echo "生成 kubernetes 证书和私钥"
cfssl gencert -ca=/root/.k8s/deploy/ca.pem \
  -ca-key=/root/.k8s/deploy/ca-key.pem \
  -config=/root/.k8s/deploy/ca-config.json \
  -profile=kubernetes kubernetes-csr.json | cfssljson -bare kubernetes

# 生成的证书和私钥文件拷贝到所有master节点
echo "生成 kuberbernetes 证书和私钥文件拷贝到所有master节点"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "mkdir -p /etc/kubernetes/ssl"
    scp kubernetes*.pem root@${node_ip}:/etc/kubernetes/ssl/
done

# 创建审计策略文件
echo "创建审计策略文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > audit-policy.yaml <<EOF
apiVersion: audit.k8s.io/v1beta1
kind: Policy
rules:
  # The following requests were manually identified as high-volume and low-risk, so drop them.
  - level: None
    resources:
      - group: ""
        resources:
          - endpoints
          - services
          - services/status
    users:
      - 'system:kube-proxy'
    verbs:
      - watch
  - level: None
    resources:
      - group: ""
        resources:
          - nodes
          - nodes/status
    userGroups:
      - 'system:nodes'
    verbs:
      - get
  - level: None
    namespaces:
      - kube-system
    resources:
      - group: ""
        resources:
          - endpoints
    users:
      - 'system:kube-controller-manager'
      - 'system:kube-scheduler'
      - 'system:serviceaccount:kube-system:endpoint-controller'
    verbs:
      - get
      - update
  - level: None
    resources:
      - group: ""
        resources:
          - namespaces
          - namespaces/status
          - namespaces/finalize
    users:
      - 'system:apiserver'
    verbs:
      - get
  # Don't log HPA fetching metrics.
  - level: None
    resources:
      - group: metrics.k8s.io
    users:
      - 'system:kube-controller-manager'
    verbs:
      - get
      - list
  # Don't log these read-only URLs.
  - level: None
    nonResourceURLs:
      - '/healthz*'
      - /version
      - '/swagger*'
  # Don't log events requests.
  - level: None
    resources:
      - group: ""
        resources:
          - events
  # node and pod status calls from nodes are high-volume and can be large, don't log responses for expected updates from nodes
  - level: Request
    omitStages:
      - RequestReceived
    resources:
      - group: ""
        resources:
          - nodes/status
          - pods/status
    users:
      - kubelet
      - 'system:node-problem-detector'
      - 'system:serviceaccount:kube-system:node-problem-detector'
    verbs:
      - update
      - patch
  - level: Request
    omitStages:
      - RequestReceived
    resources:
      - group: ""
        resources:
          - nodes/status
          - pods/status
    userGroups:
      - 'system:nodes'
    verbs:
      - update
      - patch
  # deletecollection calls can be large, don't log responses for expected namespace deletions
  - level: Request
    omitStages:
      - RequestReceived
    users:
      - 'system:serviceaccount:kube-system:namespace-controller'
    verbs:
      - deletecollection
  # Secrets, ConfigMaps, and TokenReviews can contain sensitive & binary data,
  # so only log at the Metadata level.
  - level: Metadata
    omitStages:
      - RequestReceived
    resources:
      - group: ""
        resources:
          - secrets
          - configmaps
      - group: authentication.k8s.io
        resources:
          - tokenreviews
  # Get repsonses can be large; skip them.
  - level: Request
    omitStages:
      - RequestReceived
    resources:
      - group: ""
      - group: admissionregistration.k8s.io
      - group: apiextensions.k8s.io
      - group: apiregistration.k8s.io
      - group: apps
      - group: authentication.k8s.io
      - group: authorization.k8s.io
      - group: autoscaling
      - group: batch
      - group: certificates.k8s.io
      - group: extensions
      - group: metrics.k8s.io
      - group: networking.k8s.io
      - group: policy
      - group: rbac.authorization.k8s.io
      - group: scheduling.k8s.io
      - group: settings.k8s.io
      - group: storage.k8s.io
    verbs:
      - get
      - list
      - watch
  # Default level for known APIs
  - level: RequestResponse
    omitStages:
      - RequestReceived
    resources:
      - group: ""
      - group: admissionregistration.k8s.io
      - group: apiextensions.k8s.io
      - group: apiregistration.k8s.io
      - group: apps
      - group: authentication.k8s.io
      - group: authorization.k8s.io
      - group: autoscaling
      - group: batch
      - group: certificates.k8s.io
      - group: extensions
      - group: metrics.k8s.io
      - group: networking.k8s.io
      - group: policy
      - group: rbac.authorization.k8s.io
      - group: scheduling.k8s.io
      - group: settings.k8s.io
      - group: storage.k8s.io
  # Default level for all other requests.
  - level: Metadata
    omitStages:
      - RequestReceived
EOF

# 分发审计策略文件到所有 master 节点
echo "分发审计策略文件到所有 master 节点"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp audit-policy.yaml root@${node_ip}:/etc/kubernetes/audit-policy.yaml
done

# 创建 aggregator 证书签名请求
echo "创建 aggregator 证书签名请求"
cat > aggregator-proxy-csr.json << EOF
{
    "CN": "aggregator",
    "hosts": [],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "GuangDong",
            "L": "ShenZhen",
            "O": "k8s",
            "OU": "System"
        }
    ]
}
EOF

# 生成 aggregator 证书和私钥
echo "生成 aggregator 证书和私钥"
cfssl gencert -ca=/root/.k8s/deploy/ca.pem \
  -ca-key=/root/.k8s/deploy/ca-key.pem  \
  -config=/root/.k8s/deploy/ca-config.json  \
  -profile=kubernetes aggregator-proxy-csr.json | cfssljson -bare aggregator-proxy

# 将生成的证书和私钥文件拷贝到 master 节点
echo "将生成的 aggregator-proxy 证书和私钥文件拷贝所有 master 节点"
cd /root/.k8s/deploy
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp aggregator-proxy*.pem root@${node_ip}:/etc/kubernetes/ssl/
done


# 创建kube-apiserver systemd 模板文件
echo "创建kube-apiserver systemd 模板文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > kube-apiserver.service.template <<EOF
[Unit]
Description=Kubernetes API Server
Documentation=https://github.com/GoogleCloudPlatform/kubernetes
After=network.target

[Service]
WorkingDirectory=${K8S_DIR}/kube-apiserver
ExecStart=/opt/k8s/bin/kube-apiserver \\
          --advertise-address=##NODE_IP## \\
          --allow-privileged=true \\
          --anonymous-auth=false \\
          --apiserver-count=3 \\
          --audit-dynamic-configuration \\
          --audit-log-maxage=15 \\
          --audit-log-maxbackup=3 \\
          --audit-log-maxsize=100 \\
          --audit-log-truncate-enabled \\
          --audit-log-path=${K8S_DIR}/kube-apiserver/audit.log \\
          --audit-policy-file=/etc/kubernetes/audit-policy.yaml \\
          --authorization-mode=Node,RBAC \\
          --bind-address=##NODE_IP## \\
          --client-ca-file=/etc/kubernetes/ssl/ca.pem \\
          --default-not-ready-toleration-seconds=360 \\
          --default-unreachable-toleration-seconds=360 \\
          --default-watch-cache-size=200 \\
          --delete-collection-workers=2 \\
          --enable-admission-plugins=NamespaceLifecycle,LimitRanger,ServiceAccount,DefaultStorageClass,DefaultTolerationSeconds,MutatingAdmissionWebhook,ValidatingAdmissionWebhook,ResourceQuota,NodeRestriction \\
          --enable-aggregator-routing=true \\
          --enable-bootstrap-token-auth \\
          --endpoint-reconciler-type=lease \\
          --etcd-cafile=/etc/kubernetes/ssl/ca.pem \\
          --etcd-certfile=/etc/kubernetes/ssl/kubernetes.pem \\
          --etcd-keyfile=/etc/kubernetes/ssl/kubernetes-key.pem \\
          --etcd-servers=${ETCD_ENDPOINTS} \\
          --event-ttl=200h \\
          --feature-gates=DynamicAuditing=true \\
          --feature-gates=TTLAfterFinished=true \\
          --insecure-port=0 \\
          --kubelet-certificate-authority=/etc/kubernetes/ssl/ca.pem \\
          --kubelet-client-certificate=/etc/kubernetes/ssl/kubernetes.pem \\
          --kubelet-client-key=/etc/kubernetes/ssl/kubernetes-key.pem \\
          --kubelet-https=true \\
          --kubelet-timeout=10s \\
          --logtostderr=true \\
          --max-mutating-requests-inflight=2000 \\
          --max-requests-inflight=4000 \\
          --profiling=true \\
          --proxy-client-cert-file=/etc/kubernetes/ssl/aggregator-proxy.pem \\
          --proxy-client-key-file=/etc/kubernetes/ssl/aggregator-proxy-key.pem \\
          --requestheader-allowed-names="aggregator" \\
          --requestheader-client-ca-file=/etc/kubernetes/ssl/ca.pem \\
          --requestheader-extra-headers-prefix=X-Remote-Extra- \\
          --requestheader-group-headers=X-Remote-Group \\
          --requestheader-username-headers=X-Remote-User \\
          --runtime-config=api/all=true \\
          --secure-port=6443 \\
          --service-account-key-file=/etc/kubernetes/ssl/ca.pem \\
          --service-cluster-ip-range=${SERVICE_CIDR} \\
          --service-node-port-range=${NODE_PORT_RANGE} \\
          --tls-cert-file=/etc/kubernetes/ssl/kubernetes.pem \\
          --tls-private-key-file=/etc/kubernetes/ssl/kubernetes-key.pem \\
          --v=2
Restart=on-failure
RestartSec=5
Type=notify
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target
EOF

# 各个节点创建和分发 kube-apiserver 启动文件
echo "各个节点创建和分发 kube-apiserver 启动文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for (( i=0; i < 3; i++  )); do
    sed -e "s/##NODE_NAME##/${MASTER_NAMES[i]}/" -e "s/##NODE_IP##/${MASTER_IPS[i]}/" kube-apiserver.service.template > kube-apiserver-${MASTER_IPS[i]}.service 
done

cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp kube-apiserver-${node_ip}.service root@${node_ip}:/etc/systemd/system/kube-apiserver.service
done

# 启动 kube-apiserver
echo "启动 kube-apiserver"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "mkdir -p ${K8S_DIR}/kube-apiserver"
    ssh root@${node_ip} "systemctl daemon-reload && systemctl enable kube-apiserver && systemctl stop kube-apiserver && systemctl restart kube-apiserver"
done

# 验证服务是否正常
echo "验证服务是否正常"
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "systemctl status kube-apiserver |grep 'Active:'"
done

# 打印 kube-apiserver 写入 etcd 数据
echo "打印 kube-apiserver 写入 etcd 数据"
source /opt/k8s/bin/env.sh
ETCDCTL_API=3 etcdctl \
    --endpoints=${ETCD_ENDPOINTS} \
    --cacert=/root/.k8s/deploy/ca.pem \
    --cert=/root/.k8s/deploy/etcd.pem \
    --key=/root/.k8s/deploy/etcd-key.pem \
    get /registry/ --prefix --keys-only

# 检查集群状态
echo "检查集群状态"
source /opt/k8s/bin/env.sh
kubectl cluster-info
kubectl get all --all-namespaces
kubectl get componentstatuses

# 授权 kube-apiserver 访问 kubelet API的权限
echo "授权 kube-apiserver 访问 kubelet API 权限"
source /opt/k8s/bin/env.sh
if [ $(kubectl get clusterrolebind kubelet-api-admin | wc -l) -eq 0 ]; then
    kubectl create clusterrolebinding kubelet-api-admin --clusterrole=system:kubelet-api-admin --user kubernetes
fi
