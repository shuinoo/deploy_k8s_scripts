#!/bin/bash

# 生成 ca 签名配置文件
echo "生成 ca 签名配置文件"
cat > /root/.k8s/deploy/ca-config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "87600h"
    },
    "profiles": {
      "kubernetes": {
        "usages": [
            "signing",
            "key encipherment",
            "server auth",
            "client auth"
        ],
        "expiry": "87600h"
      }
    }
  }
}
EOF

# 创建 ca 证书签名请求文件
echo "创建 ca 证书签名请求文件"
cat >/root/.k8s/deploy/ca-csr.json << EOF
{
    "CN": "kubernetes",
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "GuangDong",
            "L": "ShenZhen",
            "O": "k8s",
            "OU": "System"
        }
    ],
    "ca": {
        "expiry": "87600h"
    }
}
EOF

# 生成 ca 证书和密钥
echo "生成 ca 证书和密钥"
cd /root/.k8s/deploy/
source /opt/k8s/bin/env.sh
cfssl gencert -initca ca-csr.json | cfssljson -bare ca

# 分发 ca 证书和密钥
echo "分发 ca 证书和密钥分发 ca 证书和密钥"
cd /root/.k8s/deploy/
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]};do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "mkdir -p /etc/kubernetes/ssl"
    scp ca*.pem root@${node_ip}:/etc/kubernetes/ssl
done
