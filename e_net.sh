#!/usr/bin/env bash

function e_net() {
  set -eu
  pod=`kubectl get pod ${pod_name} -n ${namespace} -o template --template='{{range .status.containerStatuses}}{{.containerID}}{{end}}' | sed 's/docker:\/\/\(.*\)$/\1/'`
  pid=`docker inspect -f {{.State.Pid}} $pod`
  echo -e "\033[32m Entering pod netns for ${namespace}/${pod_name} \033[0m\n"
  cmd="nsenter -n -t ${pid}"
  echo -e "\033[32m Execute the command: ${cmd} \033[0m"
  ${cmd}
}

pod_name=$1
namespace=${2-"default"}
e_net
