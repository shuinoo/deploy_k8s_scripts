#!/bin/bash
# cfssl 版本
export CFSSL_VERSION="1.6.0"
# 生成 EncryptionConfig 所需的加密 key
export ENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64)
# 集群各机器 IP 数组
export NODE_IPS=(192.168.7.80 192.168.7.81 192.168.7.82)
# 集群各 IP 对应的主机名数组
export NODE_NAMES=(qh-k8s-01 qh-k8s-02 qh-k8s-03)
# 集群MASTER机器 IP 数组
export MASTER_IPS=(192.168.7.80 192.168.7.81 192.168.7.82)
# 集群所有的 master IP对应的主机
export MASTER_NAMES=(qh-k8s-01 qh-k8s-02 qh-k8s-03)
# 集群所有的 node IP对应的主机
export WORK_NAMES=(qh-k8s-01 qh-k8s-02 qh-k8s-03) 
# etcd 集群服务地址列表
export ETCD_ENDPOINTS="https://192.168.7.80:2379,https://192.168.7.81:2379,https://192.168.7.82:2379"
# etcd 版本
export ETCD_VERSION="v3.3.25"
# etcd 集群服务的主机名数组
export ETCD_NAMES=(etcd01 etcd02 etcd03)
# etcd 集群间通信的 IP 和端口
export ETCD_NODES="etcd01=https://192.168.7.80:2380,etcd02=https://192.168.7.81:2380,etcd03=https://192.168.7.82:2380"
# etcd 集群所有node ip
export ETCD_IPS=(192.168.7.80 192.168.7.81 192.168.7.82)
# kubernetes 版本
export KUBE_VERSION="1.14.10"
# 集群管理员
export KUBE_ADMIN="kubernetes-admin"
# dashboard
export DASHBOARD_IMAGE="registry.aliyuncs.com/google_containers/dashboard:v2.0.4"
# dashboard-metrics
export DASHBOARD_METRICS_IMAGE="registry.aliyuncs.com/google_containers/metrics-scraper:v1.0.4"
# metrics-server
export METRICS_SERVER_IMAGE="registry.aliyuncs.com/k8sxio/metrics-server:v0.3.7"
# 集群名
export KUBE_CLUSTER="kubernetes"
# pause镜像
export PAUSE_IMAGE="registry.aliyuncs.com/google_containers/pause:3.2"
# CNI 版本
export CNI_VERSION="v0.9.1"
# ingress-nginx 版本
export INGRESS_VERSION="v0.3.0"
# ingress-webhook 版本
export INGRESS-WEBHOOK_VERSION="v1.15.0"
# docker 版本
export DOCKER_VERSION="18.09.9"
# docker registry
export DOCKER_REGISTRY="https://vjxqk84i.mirror.aliyuncs.com"
# lb 的地址
export LB_IP="192.168.7.88"
# kube-apiserver 的反向代理(kube-nginx)地址端口
export KUBE_APISERVER="https://${LB_IP}:9443"
# 节点间互联网络接口名称
export IFACE="eth0"
# etcd 数据目录
export ETCD_DATA_DIR="/data/kubernetes/etcd/data"
# etcd WAL 目录，建议是 SSD 磁盘分区，或者和 ETCD_DATA_DIR 不同的磁盘分区
export ETCD_WAL_DIR="/data/kubernetes/etcd/wal"
# k8s 各组件数据目录
export K8S_DIR="/data/kubernetes"
# docker 数据目录
export DOCKER_DIR="/data/kubernetes/docker"
# keepalived 节点优先级
export LEVEL=(100 90 80)
## 以下参数一般不需要修改
# TLS Bootstrapping 使用的 Token，可以使用命令 head -c 16 /dev/urandom | od -An -t x | tr -d ' ' 生成
#BOOTSTRAP_TOKEN="41f7e4ba8b7be874fcff18bf5cf41a7c"
# 最好使用 当前未用的网段 来定义服务网段和 Pod 网段
# 服务网段，部署前路由不可达，部署后集群内路由可达(kube-proxy 保证)
SERVICE_CIDR="10.77.0.0/16"
# Pod 网段，建议 /16 段地址，部署前路由不可达，部署后集群内路由可达(cni 保证)
CLUSTER_CIDR="172.77.0.0/16"
# 服务端口范围 (NodePort Range)
export NODE_PORT_RANGE="30000-40000"
# flanneld 网络配置前缀
export FLANNEL_ETCD_PREFIX="/kubernetes/network"
# kubernetes 服务 IP (一般是 SERVICE_CIDR 中第一个IP)
export CLUSTER_KUBERNETES_SVC_IP="10.77.0.1"
# 集群 DNS 服务 IP (从 SERVICE_CIDR 中预分配)
export CLUSTER_DNS_SVC_IP="10.77.0.2"
# 集群 DNS 域名（末尾不带点号）
export CLUSTER_DNS_DOMAIN="cluster.local"
# coredns image 地址
export COREDNS_IMAGE="registry.aliyuncs.com/google_containers/coredns"
# coredns 服务版本
export COREDNS_VERSION="1.8.0"
# nodelocaldns
export NODELOCALDNS_ENABLE="false"
# nodelocaldns 服务 IP
export NODELOCALDNS_IP="169.254.20.10"
# nodelocaldns 服务版本
export NODELOCALDNS_VERSION="1.17.0"
# 将二进制目录 /opt/k8s/bin 加到 PATH 中
export PATH=/opt/k8s/bin:$PATH
# server
export Download_Server="http://192.168.7.59"
