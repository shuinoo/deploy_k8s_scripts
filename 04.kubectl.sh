#!/bin/bash 

# 下载 kubectl 二进制文件 
echo "下载kubectl 二进制文件"
source /opt/k8s/bin/env.sh
cd /root/.k8s/deploy
[ ! -f /root/.k8s/deploy/kubernetes-client-linux-amd64.tar.gz ] && wget ${Download_Server}/linux/kubernetes/${KUBE_VERSION}/kubernetes-client-linux-amd64.tar.gz
tar xf kubernetes-client-linux-amd64.tar.gz

# 分发 kubectl 到所有节点
echo "分发 kubectl 到所有节点"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp kubernetes/client/bin/kubectl root@${node_ip}:/opt/k8s/bin/
    ssh root@${node_ip} "chmod +x /opt/k8s/bin/*"
done
