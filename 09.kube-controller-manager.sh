#!/bin/bash

# 创建 kube-controller-manager 证书签名请求
echo "创建 kube-controller-manager 证书签名请求"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > kube-controller-manager-csr.json << EOF
{
    "CN": "system:kube-controller-manager",
    "hosts": [
        "127.0.0.1",
        "${MASTER_IPS[0]}",
        "${MASTER_IPS[1]}",
        "${MASTER_IPS[2]}"
    ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "GuangDong",
            "L": "ShenZhen",
            "O": "system:kube-controller-manager",
            "OU": "System"
        }
    ]
}
EOF

# 生成 kube-controller-manager 证书和私钥
echo "生成 kube-controller-manager 证书和私钥"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cfssl gencert -ca=/root/.k8s/deploy/ca.pem \
  -ca-key=/root/.k8s/deploy/ca-key.pem \
  -config=/root/.k8s/deploy/ca-config.json \
  -profile=kubernetes kube-controller-manager-csr.json | cfssljson -bare kube-controller-manager

# 分发生成证书和私钥到所有 master 节点
echo "分发生成证书和私钥到所有 master 节点"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp kube-controller-manager*.pem root@${node_ip}:/etc/kubernetes/ssl/
done

# 创建 kube-controller-manager kubeconfig 文件
echo "创建 kube-controller-manager kubeconfig 文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
kubectl config set-cluster ${KUBE_CLUSTER} \
  --certificate-authority=/root/.k8s/deploy/ca.pem \
  --embed-certs=true \
  --server=${KUBE_APISERVER} \
  --kubeconfig=kube-controller-manager.kubeconfig
kubectl config set-credentials system:kube-controller-manager \
  --client-certificate=kube-controller-manager.pem \
  --client-key=kube-controller-manager-key.pem \
  --embed-certs=true \
  --kubeconfig=kube-controller-manager.kubeconfig
kubectl config set-context system:kube-controller-manager \
  --cluster=${KUBE_CLUSTER} \
  --user=system:kube-controller-manager \
  --kubeconfig=kube-controller-manager.kubeconfig
kubectl config use-context system:kube-controller-manager --kubeconfig=kube-controller-manager.kubeconfig

# 分发 kube-controller-manager kubeconfig 到所有 master 节点
echo "分发 kube-controller-manager kubeconfig 到所有 master 节点"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp kube-controller-manager.kubeconfig root@${node_ip}:/etc/kubernetes/
done

# 创建 kube-controller-manager systemd 
echo "分发 kube-controller-manager systemd"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > kube-controller-manager.service.template <<EOF
[Unit]
Description=Kubernetes Controller Manager
Documentation=https://github.com/GoogleCloudPlatform/kubernetes

[Service]
WorkingDirectory=${K8S_DIR}/kube-controller-manager
ExecStart=/opt/k8s/bin/kube-controller-manager \\
          --allocate-node-cidrs=true \\
          --authentication-kubeconfig=/etc/kubernetes/kube-controller-manager.kubeconfig \\
          --authorization-kubeconfig=/etc/kubernetes/kube-controller-manager.kubeconfig \\
          --bind-address=0.0.0.0 \\
          --client-ca-file=/etc/kubernetes/ssl/ca.pem \\
          --cluster-name=${KUBE_CLUSTER} \\
          --cluster-signing-cert-file=/etc/kubernetes/ssl/ca.pem \\
          --cluster-signing-key-file=/etc/kubernetes/ssl/ca-key.pem \\
          --cluster-cidr=${CLUSTER_CIDR} \\
          --controllers=*,bootstrapsigner,tokencleaner \\
          --concurrent-deployment-syncs=10 \\
          --concurrent-gc-syncs=30 \\
          --concurrent-service-syncs=2 \\
          --experimental-cluster-signing-duration=867000h \\
          --feature-gates=RotateKubeletServerCertificate=true \\
          --feature-gates=RotateKubeletClientCertificate=true \\
          --feature-gates=TTLAfterFinished=true \\
          --horizontal-pod-autoscaler-sync-period=10s \\
          --kube-api-burst=2000 \\
          --kube-api-qps=1000 \\
          --kubeconfig=/etc/kubernetes/kube-controller-manager.kubeconfig \\
          --leader-elect=true \\
          --logtostderr=true \\
          --node-cidr-mask-size=24 \\
          --pod-eviction-timeout=6m \\
          --profiling=true \\
          --requestheader-client-ca-file=/etc/kubernetes/ssl/ca.pem \\
          --requestheader-extra-headers-prefix="X-Remote-Extra-" \\
          --requestheader-group-headers=X-Remote-Group \\
          --requestheader-username-headers=X-Remote-User \\
          --root-ca-file=/etc/kubernetes/ssl/ca.pem \\
          --service-account-private-key-file=/etc/kubernetes/ssl/ca-key.pem \\
          --service-cluster-ip-range=${SERVICE_CIDR} \\
          --terminated-pod-gc-threshold=10000 \\
          --tls-cert-file=/etc/kubernetes/ssl/kube-controller-manager.pem \\
          --tls-private-key-file=/etc/kubernetes/ssl/kube-controller-manager-key.pem \\
          --use-service-account-credentials=true \\
          --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

# 替换 kube-controller-manager systemd 模板文件
echo "替换 kube-controller-manager systemd 模板文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for (( i=0; i < 3; i++ )); do
    sed -e "s/##NODE_NAME##/${MASTER_NAMES[i]}/" -e "s/##NODE_IP##/${MASTER_IPS[i]}/" kube-controller-manager.service.template > kube-controller-manager-${MASTER_IPS[i]}.service 
done

# 分发替换 kube-controller-manager systemd 到各节点
echo "分发替换 kube-controller-manager systemd 到各节点"
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp kube-controller-manager-${node_ip}.service root@${node_ip}:/etc/systemd/system/kube-controller-manager.service
done

# 启动 kube-controller-manager 
echo "启动 kube-controller-manager"
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "mkdir -p ${K8S_DIR}/kube-controller-manager"
    ssh root@${node_ip} "systemctl daemon-reload && systemctl enable kube-controller-manager && systemctl stop kube-controller-manager && systemctl restart kube-controller-manager"
done

# 检查 kube-controller-manager 运行状态
echo "检查 kube-controller-manager 运行状态"
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "systemctl status kube-controller-manager|grep Active"
done
