#!/bin/bash 

# 创建 kube-proxy 证书签名请求
echo "创建 kube-proxy 证书签名请求"
cd /root/.k8s/deploy
cat > kube-proxy-csr.json << EOF
{
  "CN": "system:kube-proxy",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "CN",
      "ST": "GuangDong",
      "L": "ShenZhen",
      "O": "k8s",
      "OU": "System"
    }
  ]
}
EOF

# 生成 kube-proxy 证书和私钥
echo "生成 kube-proxy 证书和私钥"
cd /root/.k8s/deploy
cfssl gencert -ca=/root/.k8s/deploy/ca.pem \
  -ca-key=/root/.k8s/deploy/ca-key.pem \
  -config=/root/.k8s/deploy/ca-config.json \
  -profile=kubernetes  kube-proxy-csr.json | cfssljson -bare kube-proxy

# 创建 kube-proxy kubeconfig 文件
echo "创建 kube-proxy kubeconfig 文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
kubectl config set-cluster ${KUBE_CLUSTER} \
  --certificate-authority=/root/.k8s/deploy/ca.pem \
  --embed-certs=true \
  --server=${KUBE_APISERVER} \
  --kubeconfig=kube-proxy.kubeconfig
kubectl config set-credentials kube-proxy \
  --client-certificate=kube-proxy.pem \
  --client-key=kube-proxy-key.pem \
  --embed-certs=true \
  --kubeconfig=kube-proxy.kubeconfig
kubectl config set-context default \
  --cluster=${KUBE_CLUSTER} \
  --user=kube-proxy \
  --kubeconfig=kube-proxy.kubeconfig
kubectl config use-context default --kubeconfig=kube-proxy.kubeconfig

# 分发 kube-proxy kubeconfig 到所有节点
echo "分发 kube-proxy kubeconfig 到所有节点"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_name in ${NODE_NAMES[@]}; do
    echo ">>> ${node_name}"
    scp kube-proxy.kubeconfig root@${node_name}:/etc/kubernetes/
done

# 创建 kube-proxy 配置模板文件
echo "创建 kube-proxy 配置模板文件"
cd /root/.k8s/deploy
cat > kube-proxy-config.yaml.template << EOF
kind: KubeProxyConfiguration
apiVersion: kubeproxy.config.k8s.io/v1alpha1
clientConnection:
  burst: 200
  kubeconfig: "/etc/kubernetes/kube-proxy.kubeconfig"
  qps: 100
bindAddress: ##NODE_IP##
healthzBindAddress: ##NODE_IP##:10256
metricsBindAddress: ##NODE_IP##:10249
enableProfiling: true
clusterCIDR: ${CLUSTER_CIDR}
featureGates:
  TTLAfterFinished: true
hostnameOverride: ##NODE_NAME##
mode: "ipvs"
portRange: ""
kubeProxyIPTablesConfiguration:
  masqueradeAll: false
kubeProxyIPVSConfiguration:
  scheduler: rr
  excludeCIDRs: []
EOF

# 生成并分发各个节点的 kube-proxy 配置文件
echo "生成并分发各个节点的 kube-proxy 配置文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for (( i=0; i < 3; i++ ))
  do 
    echo ">>> ${NODE_NAMES[i]}"
    sed -e "s/##NODE_NAME##/${NODE_NAMES[i]}/" -e "s/##NODE_IP##/${NODE_IPS[i]}/" kube-proxy-config.yaml.template > kube-proxy-config-${NODE_NAMES[i]}.yaml.template
    scp kube-proxy-config-${NODE_NAMES[i]}.yaml.template root@${NODE_NAMES[i]}:/etc/kubernetes/kube-proxy-config.yaml
done

# 创建 kube-proxy systemd 文件
echo "创建 kube-proxy systemd 文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > kube-proxy.service << EOF
[Unit]
Description=Kubernetes Kube-Proxy Server
Documentation=https://github.com/GoogleCloudPlatform/kubernetes
After=network.target

[Service]
WorkingDirectory=${K8S_DIR}/kube-proxy
ExecStart=/opt/k8s/bin/kube-proxy \\
  --config=/etc/kubernetes/kube-proxy-config.yaml \\
  --logtostderr=true \\
  --v=2
LimitNOFILE=65536
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

# 分发 kube-proxy systemd 文件
echo "分发 kube-proxy systemd 文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_name in ${NODE_NAMES[@]}; do 
    echo ">>> ${node_name}"
    scp kube-proxy.service root@${node_name}:/etc/systemd/system/
done

# 启动 kube-proxy
echo "启动 kube-proxy"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "mkdir -p ${K8S_DIR}/kube-proxy"
    ssh root@${node_ip} "modprobe ip_vs_rr"
    ssh root@${node_ip} "systemctl daemon-reload && systemctl enable kube-proxy && systemctl stop kube-proxy && systemctl restart kube-proxy"
done

# 检查 kube-proxy 运行状态
echo "检查 kube-proxy 运行状态"
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "systemctl status kube-proxy | grep Active"
done

# 查看 ipvs 路由规则
echo "查看 ipvs 路由规则"
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "/usr/sbin/ipvsadm -ln"
done
