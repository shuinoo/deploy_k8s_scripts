#!/bin/bash

# 生成 nodelocaldns yaml 文件
echo "生成 nodelocaldns yaml 文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
echo "安装 nodelocaldns"
if [[ "${NODELOCALDNS_ENABLE}"  == "true" ]]; then
cat > nodelocaldns.yaml << EOF
# Copyright 2018 The Kubernetes Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

apiVersion: v1
kind: ServiceAccount
metadata:
  name: node-local-dns
  namespace: kube-system
  labels:
    kubernetes.io/cluster-service: "true"
    addonmanager.kubernetes.io/mode: Reconcile
---
apiVersion: v1
kind: Service
metadata:
  name: kube-dns-upstream
  namespace: kube-system
  labels:
    k8s-app: kube-dns
    kubernetes.io/cluster-service: "true"
    addonmanager.kubernetes.io/mode: Reconcile
    kubernetes.io/name: "KubeDNSUpstream"
spec:
  ports:
  - name: dns
    port: 53
    protocol: UDP
    targetPort: 53
  - name: dns-tcp
    port: 53
    protocol: TCP
    targetPort: 53
  selector:
    k8s-app: kube-dns
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: node-local-dns
  namespace: kube-system
  labels:
    addonmanager.kubernetes.io/mode: Reconcile
data:
  Corefile: |
    cluster.local:53 {
        errors
        cache {
                success 9984 30
                denial 9984 5
        }
        reload
        loop
        bind ${NODELOCALDNS_IP}
        forward . ${CLUSTER_DNS_SVC_IP} {
                force_tcp
        }
        prometheus :9253
        health ${NODELOCALDNS_IP}:8080
        }
    in-addr.arpa:53 {
        errors
        cache 30
        reload
        loop
        bind ${NODELOCALDNS_IP}
        forward . ${CLUSTER_DNS_SVC_IP} {
                force_tcp
        }
        prometheus :9253
        }
    ip6.arpa:53 {
        errors
        cache 30
        reload
        loop
        bind ${NODELOCALDNS_IP}
        forward . ${CLUSTER_DNS_SVC_IP} {
                force_tcp
        }
        prometheus :9253
        }
    .:53 {
        errors
        cache 30
        reload
        loop
        bind ${NODELOCALDNS_IP}
        forward . /etc/resolv.conf {
                force_tcp
        }
        prometheus :9253
        }
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: node-local-dns
  namespace: kube-system
  labels:
    k8s-app: node-local-dns
    kubernetes.io/cluster-service: "true"
    addonmanager.kubernetes.io/mode: Reconcile
spec:
  updateStrategy:
    rollingUpdate:
      maxUnavailable: 10%
  selector:
    matchLabels:
      k8s-app: node-local-dns
  template:
    metadata:
      labels:
        k8s-app: node-local-dns
      annotations:
        prometheus.io/port: "9253"
        prometheus.io/scrape: "true"
    spec:
      priorityClassName: system-node-critical
      serviceAccountName: node-local-dns
      hostNetwork: true
      dnsPolicy: Default  # Don't use cluster DNS.
      tolerations:
      - key: "CriticalAddonsOnly"
        operator: "Exists"
      - effect: "NoExecute"
        operator: "Exists"
      - effect: "NoSchedule"
        operator: "Exists"
      containers:
      - name: node-cache
        image: registry.aliyuncs.com/k8sxio/k8s-dns-node-cache:${NODELOCALDNS_VERSION}
        resources:
          requests:
            cpu: 25m
            memory: 5Mi
        args: [ "-localip", "${NODELOCALDNS_IP}", "-conf", "/etc/Corefile", "-upstreamsvc", "kube-dns-upstream" ]
        securityContext:
          privileged: true
        ports:
        - containerPort: 53
          name: dns
          protocol: UDP
        - containerPort: 53
          name: dns-tcp
          protocol: TCP
        - containerPort: 9253
          name: metrics
          protocol: TCP
        livenessProbe:
          httpGet:
            host: ${NODELOCALDNS_IP}
            path: /health
            port: 8080
          initialDelaySeconds: 60
          timeoutSeconds: 5
        volumeMounts:
        - mountPath: /run/xtables.lock
          name: xtables-lock
          readOnly: false
        - name: config-volume
          mountPath: /etc/coredns
        - name: kube-dns-config
          mountPath: /etc/kube-dns
      volumes:
      - name: xtables-lock
        hostPath:
          path: /run/xtables.lock
          type: FileOrCreate
      - name: kube-dns-config
        configMap:
          name: kube-dns
          optional: true
      - name: config-volume
        configMap:
          name: node-local-dns
          items:
            - key: Corefile
              path: Corefile.base
---
# A headless service is a service with a service IP but instead of load-balancing it will return the IPs of our associated Pods.
# We use this to expose metrics to Prometheus.
apiVersion: v1
kind: Service
metadata:
  annotations:
    prometheus.io/port: "9253"
    prometheus.io/scrape: "true"
  labels:
    k8s-app: node-local-dns
  name: node-local-dns
  namespace: kube-system
spec:
  clusterIP: None
  ports:
    - name: metrics
      port: 9253
      targetPort: 9253
  selector:
    k8s-app: node-local-dns
EOF
    kubectl apply -f /root/.k8s/deploy/nodelocaldns.yaml
fi

# 生成 coredns yaml 文件
echo "生成 coredns yaml 文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > coredns.yaml << EOF
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: coredns-autoscaler
  namespace: kube-system
  labels:
    k8s-app: coredns-autoscaler
    kubernetes.io/cluster-service: "true"
    kubernetes.io/name: "kube-dns"
    app.kubernetes.io/name: coredns-autoscaler
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: coredns
  namespace: kube-system
  labels:
    k8s-app: "kube-dns"
    kubernetes.io/name: "kube-dns"
    app.kubernetes.io/name: kube-dns
---
kind: ConfigMap
apiVersion: v1
metadata:
  name: coredns-autoscaler
  namespace:  kube-system
  labels:
    k8s-app: coredns-autoscaler
    kubernetes.io/cluster-service: "true"
    kubernetes.io/name: "kube-dns"
    app.kubernetes.io/name: coredns-autoscaler
data:
  # When cluster is using large nodes(with more cores), "coresPerReplica" should dominate.
  # If using small nodes, "nodesPerReplica" should dominate.
  linear: |-
    {
      "coresPerReplica": 1,
      "nodesPerReplica": 2,
      "preventSinglePointFailure": true,
      "min": 1,
      "max": 3,
      "includeUnschedulableNodes": false
    }
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: kube-dns
  namespace: kube-system
  labels:
    k8s-app: "kube-dns"
    kubernetes.io/cluster-service: "true"
    kubernetes.io/name: "kube-dns"
    app.kubernetes.io/name: kube-dns
data:
  Corefile: |-
    .:53 {
        errors
        health {
            lameduck 5s
        }
        ready
        kubernetes ${CLUSTER_DNS_DOMAIN} in-addr.arpa ip6.arpa {
            pods insecure
            fallthrough in-addr.arpa ip6.arpa
            ttl 30
        }
        prometheus 0.0.0.0:9153
        forward . /etc/resolv.conf
        cache 30
        loop
        reload
        loadbalance
    }
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: coredns-autoscaler
  labels:
    k8s-app: coredns-autoscaler
    kubernetes.io/cluster-service: "true"
    kubernetes.io/name: "kube-dns"
    app.kubernetes.io/name: coredns-autoscaler
rules:
  - apiGroups: [""]
    resources: ["nodes"]
    verbs: ["list","watch"]
  - apiGroups: [""]
    resources: ["replicationcontrollers/scale"]
    verbs: ["get", "update"]
  - apiGroups: ["extensions", "apps"]
    resources: ["deployments/scale", "replicasets/scale"]
    verbs: ["get", "update"]
# Remove the configmaps rule once below issue is fixed:
# kubernetes-incubator/cluster-proportional-autoscaler#16
  - apiGroups: [""]
    resources: ["configmaps"]
    verbs: ["get", "create"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: coredns
  labels:
    k8s-app: "kube-dns"
    kubernetes.io/cluster-service: "true"
    kubernetes.io/name: "kube-dns"
    app.kubernetes.io/name: kube-dns
rules:
- apiGroups:
  - ""
  resources:
  - endpoints
  - services
  - pods
  - namespaces
  verbs:
  - list
  - watch
- apiGroups:
  - discovery.k8s.io
  resources:
  - endpointslices
  verbs:
  - list
  - watch
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: coredns-autoscaler
  labels:
    k8s-app: coredns-autoscaler
    kubernetes.io/cluster-service: "true"
    kubernetes.io/name: "kube-dns"
    app.kubernetes.io/name: coredns-autoscaler
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: coredns-autoscaler
subjects:
- kind: ServiceAccount
  name: coredns-autoscaler
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: coredns
  labels:
    k8s-app: "kube-dns"
    kubernetes.io/cluster-service: "true"
    kubernetes.io/name: "kube-dns"
    app.kubernetes.io/name: kube-dns
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: coredns
subjects:
- kind: ServiceAccount
  name: coredns
  namespace: kube-system
---
apiVersion: v1
kind: Service
metadata:
  name: kube-dns
  namespace: kube-system
  labels:
    k8s-app: "kube-dns"
    kubernetes.io/cluster-service: "true"
    kubernetes.io/name: "kube-dns"
    app.kubernetes.io/name: kube-dns
  annotations:
    prometheus.io/port: "9153"
    prometheus.io/scrape: "true"
spec:
  selector:
    k8s-app: "kube-dns"
    app.kubernetes.io/name: kube-dns
  clusterIP: ${CLUSTER_DNS_SVC_IP}
  ports:
  - {port: 53, protocol: UDP, name: udp-53}
  - {port: 53, protocol: TCP, name: tcp-53}
  type: ClusterIP
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: coredns-autoscaler
  namespace: kube-system
  labels:
    k8s-app: coredns-autoscaler
    kubernetes.io/cluster-service: "true"
    kubernetes.io/name: "kube-dns"
    app.kubernetes.io/name: coredns-autoscaler
spec:
  selector:
    matchLabels:
      k8s-app: coredns-autoscaler
      app.kubernetes.io/name: coredns-autoscaler
  template:
    metadata:
      labels:
        k8s-app: coredns-autoscaler
        app.kubernetes.io/name: coredns-autoscaler
      annotations:
        checksum/configmap: c608379a7d724ba481ce60a9b16665c3cbef96738abeebc7f3e838af99934ce1
        scheduler.alpha.kubernetes.io/critical-pod: ''
        scheduler.alpha.kubernetes.io/tolerations: '[{"key":"CriticalAddonsOnly", "operator":"Exists"}]'
    spec:
      serviceAccountName: coredns-autoscaler
      containers:
      - name: autoscaler
        image: "registry.cn-hangzhou.aliyuncs.com/qcloud-gcr/cluster-proportional-autoscaler-amd64:${COREDNS_VERSION}"
        imagePullPolicy: IfNotPresent
        resources:
          limits:
            cpu: 100m
            memory: 100Mi
          requests:
            cpu: 20m
            memory: 10Mi
        command:
          - /cluster-proportional-autoscaler
          - --namespace=kube-system
          - --configmap=coredns-autoscaler
          - --target=Deployment/coredns
          - --logtostderr=true
          - --v=2
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: coredns
  namespace: kube-system
  labels:
    k8s-app: "kube-dns"
    kubernetes.io/cluster-service: "true"
    kubernetes.io/name: "kube-dns"
    app.kubernetes.io/name: kube-dns
    app.kubernetes.io/version: "1.8.0"
spec:
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 1
      maxSurge: 25%
  selector:
    matchLabels:
      k8s-app: "kube-dns"
      app.kubernetes.io/name: kube-dns
  template:
    metadata:
      labels:
        k8s-app: "kube-dns"
        app.kubernetes.io/name: kube-dns
      annotations:
        checksum/config: 02c19cbc1e9d1c8e4678c4a6171023cb0967fa8d559ac01e9325e6cc8c448bb5
        scheduler.alpha.kubernetes.io/critical-pod: ''
        scheduler.alpha.kubernetes.io/tolerations: '[{"key":"CriticalAddonsOnly", "operator":"Exists"}]'
    spec:
      terminationGracePeriodSeconds: 30
      serviceAccountName: coredns
      dnsPolicy: Default
      containers:
      - name: "coredns"
        image: "${COREDNS_IMAGE}:${COREDNS_VERSION}"
        imagePullPolicy: IfNotPresent
        args: [ "-conf", "/etc/coredns/Corefile" ]
        volumeMounts:
        - name: config-volume
          mountPath: /etc/coredns
        resources:
          limits:
            cpu: 100m
            memory: 128Mi
          requests:
            cpu: 100m
            memory: 128Mi
        ports:
        - {containerPort: 53, protocol: UDP, name: udp-53}
        - {containerPort: 53, protocol: TCP, name: tcp-53}
        livenessProbe:
          httpGet:
            path: /health
            port: 8080
            scheme: HTTP
          initialDelaySeconds: 60
          periodSeconds: 10
          timeoutSeconds: 5
          successThreshold: 1
          failureThreshold: 5
        readinessProbe:
          httpGet:
            path: /ready
            port: 8181
            scheme: HTTP
          initialDelaySeconds: 30
          periodSeconds: 10
          timeoutSeconds: 5
          successThreshold: 1
          failureThreshold: 5
      volumes:
        - name: config-volume
          configMap:
            name: kube-dns
            items:
            - key: Corefile
              path: Corefile
EOF

# 安装 coredns 
echo "安装 coredns"
cd /root/.k8s/deploy
kubectl apply -f /root/.k8s/deploy/coredns.yaml

# 测试dns解析
echo "测试dns解析"
kubectl run dnstools -it --rm --restart=Never --image=infoblox/dnstools:latest --command nslookup kubernetes
