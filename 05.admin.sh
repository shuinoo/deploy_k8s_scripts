#!/bin/bash 

# 创建  证书签名请求
echo "创建  证书签名请求"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > ${KUBE_ADMIN}-csr.json <<EOF
{
    "CN": "${KUBE_ADMIN}",
    "hosts": [],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "GuangDong",
            "L": "ShenZhen",
            "O": "system:masters",
            "OU": "System"
        }
    ]
}
EOF

# 生成${KUBE_ADMIN}证书和私钥
echo "生成${KUBE_ADMIN}证书和私钥"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cfssl gencert -ca=/root/.k8s/deploy/ca.pem \
  -ca-key=/root/.k8s/deploy/ca-key.pem \
  -config=/root/.k8s/deploy/ca-config.json \
  -profile=kubernetes ${KUBE_ADMIN}-csr.json | cfssljson -bare ${KUBE_ADMIN}


# 生成 kubectl kubeconfig
echo "生成 kubectl kubeconfig"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
# 设置集群参数
kubectl config set-cluster ${KUBE_CLUSTER} \
  --certificate-authority=/root/.k8s/deploy/ca.pem \
  --embed-certs=true \
  --server=${KUBE_APISERVER} \
  --kubeconfig=kubectl.kubeconfig
#设置客户端认证参数
kubectl config set-credentials ${KUBE_ADMIN} \
  --client-certificate=/root/.k8s/deploy/${KUBE_ADMIN}.pem \
  --client-key=/root/.k8s/deploy/${KUBE_ADMIN}-key.pem \
  --embed-certs=true \
  --kubeconfig=kubectl.kubeconfig
# 设置上下文参数
kubectl config set-context ${KUBE_CLUSTER} \
  --cluster=${KUBE_CLUSTER} \
  --user=${KUBE_ADMIN} \
  --kubeconfig=kubectl.kubeconfig
# 设置默认上下文
kubectl config use-context ${KUBE_CLUSTER} --kubeconfig=kubectl.kubeconfig


# 分发 kubectl kubeconfig 到所有节点
echo "分发 kubectl kubeconfig 到所有节点"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "mkdir -p ~/.kube"
    scp kubectl.kubeconfig root@${node_ip}:~/.kube/config
done
