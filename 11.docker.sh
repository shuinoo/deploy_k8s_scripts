#!/bin/bash 

# 安装 docker 所需的包
echo "安装 docker 所需的包"
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "yum install -y yum-utils device-mapper-persistent-data lvm2"
done


# 生成 daemon.json 文件
echo "生成 daemon.json 文件"
cd /root/.k8s/deploy
cat > daemon.json << EOF
{
  "registry-mirrors": ["${DOCKER_REGISTRY}"],
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "data-root": "/data/docker",
  "storage-driver": "overlay2"
}
EOF

# docker-ce repo文件
echo "docker-ce repo文件"
cd /root/.k8s/deploy
cat > docker-ce.repo << EOF
[docker-ce-stable]
name=Docker CE Stable - Sources
baseurl="https://mirrors.bfsu.edu.cn/docker-ce/linux/centos/7/\$basearch/stable"
enabled=1
gpgcheck=1
gpgkey=https://mirrors.bfsu.edu.cn/docker-ce/linux/centos/gpg
EOF

# 安装docker-ce
echo "安装docker-ce"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp docker-ce.repo root@${node_ip}:/etc/yum.repos.d/docker-ce.repo
    ssh root@${node_ip} "yum -y install docker-ce-${DOCKER_VERSION}"
done

# 分发生成 daemon.json 文件到所有节点
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "mkdir -p /data/docker /etc/docker"
    scp daemon.json root@${node_ip}:/etc/docker/daemon.json
done

# 启动 docker
echo "启动 docker"
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "systemctl daemon-reload && systemctl enable docker && systemctl stop docker && systemctl restart docker"
done

# 检查 docker 运行状态
echo "检查 docker 运行状态"
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "systemctl status docker | grep Active"
done

# 检查是否生成 docker0 网桥
echo "检查是否生成 docker0 网桥"
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "/usr/sbin/ip addr show docker0"
done

# 输出所有节点 docker 信息
echo "输出所有节点 docker 信息"
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "docker info"
done
