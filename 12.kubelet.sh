#!/bin/bash

# 下载 kubernetes node 组件
echo "下载 kubernetes node 组件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
[ ! -f /root/.k8s/deploy/kubernetes-node-linux-amd64.tar.gz ] && wget ${Download_Server}/linux/kubernetes/${KUBE_VERSION}/kubernetes-node-linux-amd64.tar.gz
tar xf kubernetes-node-linux-amd64.tar.gz

# 分发 kubelet kube-proxy 到所有节点
echo "分发 kubelet kube-proxy 到所有节点"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "systemctl stop kubelet kube-proxy"
    scp kubernetes/node/bin/{kubeadm,kubelet,kube-proxy} root@${node_ip}:/opt/k8s/bin
done

# 下载 cni 二进制文件
echo "下载 cni 二进制文件"
mkdir /root/.k8s/deploy/cni
cd /root/.k8s/deploy/cni
[ ! -f /root/.k8s/deploy/cni/cni-plugins-linux-amd64-${CNI_VERSION}.tgz ] && wget ${Download_Server}/linux/cni-plugins/cni-plugins-linux-amd64-${CNI_VERSION}.tgz
tar xf cni-plugins-linux-amd64-${CNI_VERSION}.tgz

# 分发 cni 二进制文件至所有节点
echo "分发 cni 二进制文件至所有节点"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "mkdir -p /opt/cni/bin"
    scp cni/{bridge,host-local,loopback} root@${node_ip}:/opt/cni/bin
done

# 生成 cni 配置文件
echo "生成 cni 配置文件"
cd /root/.k8s/deploy/cni
cat > cni-default.conf << EOF
{
  "cniVersion": "0.3.0",
  "name": "mynet",
  "type": "bridge",
  "bridge": "mynet0",
  "isGateway": true,
  "ipMasq": true,
  "hairpinMode": true,
  "ipam": {
            "type": "host-local",
            "subnet": "${CLUSTER_CIDR}"
        }
}
EOF

# 分发 cni 配置文件到所有节点
echo "分发 cni 配置文件到所有节点"
cd /root/.k8s/deploy/cni
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "mkdir -p /etc/cni/net.d"
    scp cni-default.conf root@${node_ip}:/etc/cni/net.d/
done

# 生成 kubelet bootstrap kubeconfig 文件
echo "生成 kubelet bootstrap kubeconfig 文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_name in ${NODE_NAMES[@]}; do
    echo ">>> ${node_name}"
    # 创建 token
    export BOOTSTRAP_TOKEN=$(kubeadm token create \
      --description kubelet-bootstrap-token \
      --groups system:bootstrappers:${node_name} \
      --kubeconfig ~/.kube/config)
    # 设置集群参数
    kubectl config set-cluster ${KUBE_CLUSTER} \
      --certificate-authority=/etc/kubernetes/ssl/ca.pem \
      --embed-certs=true \
      --server=${KUBE_APISERVER} \
      --kubeconfig=kubelet-bootstrap-${node_name}.kubeconfig
    # 设置客户端认证参数
    kubectl config set-credentials kubelet-bootstrap \
      --token=${BOOTSTRAP_TOKEN} \
      --kubeconfig=kubelet-bootstrap-${node_name}.kubeconfig
    # 设置上下文参数
    kubectl config set-context default \
      --cluster=${KUBE_CLUSTER} \
      --user=kubelet-bootstrap \
      --kubeconfig=kubelet-bootstrap-${node_name}.kubeconfig
    # 设置默认上下文
    kubectl config use-context default --kubeconfig=kubelet-bootstrap-${node_name}.kubeconfig
done

# 查看 kubeadm 为各个节点创建的 token
echo "查看 kubeadm 为各个节点创建的token"
source /opt/k8s/bin/env.sh
kubeadm token list --kubeconfig ~/.kube/config

# 查看各 token 关联的 Secret
echo "查看各 token 关联的 Secret"
source /opt/k8s/bin/env.sh
/opt/k8s/bin/kubectl get secrets -n kube-system | grep bootstrap-token

# 分发各节点 kubelet bootstrap kubeconfig 文件
echo "分发各节点 kubelet bootstrap kubeconfig 文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_name in ${NODE_NAMES[@]}; do
    echo ">>> ${node_name}"
    scp kubelet-bootstrap-${node_name}.kubeconfig root@${node_name}:/etc/kubernetes/kubelet-bootstrap.kubeconfig
done

# 创建 kubelet 模板配置文件
echo "创建 kubelet 模板配置文件"
cd /root/.k8s/deploy
cat > kubelet-config.yaml.template << EOF
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
address: "##NODE_IP##"
authentication:
  anonymous:
    enabled: false
  webhook:
    cacheTTL: 2m0s
    enabled: true
  x509:
    clientCAFile: "/etc/kubernetes/ssl/ca.pem"
authorization:
  mode: Webhook
  webhook:
    cacheAuthorizedTTL: 5m0s
    cacheUnauthorizedTTL: 30s
cgroupDriver: systemd
cgroupsPerQOS: true
clusterDNS:
  - "${CLUSTER_DNS_SVC_IP}"
clusterDomain: "${CLUSTER_DNS_DOMAIN}"
configMapAndSecretChangeDetectionStrategy: Watch
containerLogMaxSize: 20Mi
containerLogMaxFiles: 10
enforceNodeAllocatable: ["pods"]
eventBurst: 20
eventRecordQPS: 5
evictionHard:
  memory.available: "100Mi"
  nodefs.available: "10%"
  nodefs.inodesFree: "5%"
  imagefs.available: "15%"
evictionPressureTransitionPeriod: 5m0s
failSwapOn: true
featureGates:
  RotateKubeletServerCertificate: true
  RotateKubeletClientCertificate: true
  TTLAfterFinished: true
fileCheckFrequency: 20s
hairpinMode: hairpin-veth
healthzBindAddress: "##NODE_IP##"
healthzPort: 10248
httpCheckFrequency: 20s
kubeReserved: {}
kubeAPIBurst: 2000
kubeAPIQPS: 1000
maxPods: 220
podCIDR: "${CLUSTER_CIDR}"
port: 10250
readOnlyPort: 0
resolvConf: /etc/resolv.conf
rotateCertificates: true
serializeImagePulls: true
serverTLSBootstrap: true
EOF

# 为各个节点创建和分发 kubelet 配置文件
echo "为各个节点创建和分发 kubelet 配置文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    sed -e "s/##NODE_IP##/${node_ip}/" kubelet-config.yaml.template > kubelet-config-${node_ip}.yaml.template
    if [[ "${NODELOCALDNS_ENABLE}" == "true" ]]; then
        sed -i 's/'"${CLUSTER_DNS_SVC_IP}"'/'"${NODELOCALDNS_IP}"'/' kubelet-config-${node_ip}.yaml.template
    fi
    scp kubelet-config-${node_ip}.yaml.template root@${node_ip}:/etc/kubernetes/kubelet-config.yaml
done

# 创建 kubelet systemd 模板文件
echo "创建 kubelet systemd 模板文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > kubelet.service.template <<EOF
[Unit]
Description=Kubernetes Kubelet
Documentation=https://github.com/GoogleCloudPlatform/kubernetes
After=docker.service
Requires=docker.service

[Service]
WorkingDirectory=${K8S_DIR}/kubelet
ExecStart=/opt/k8s/bin/kubelet \\
  --bootstrap-kubeconfig=/etc/kubernetes/kubelet-bootstrap.kubeconfig \\
  --cert-dir=/etc/kubernetes/ssl \\
  --cni-bin-dir=/opt/cni/bin \\
  --cni-conf-dir=/etc/cni/net.d \\
  --config=/etc/kubernetes/kubelet-config.yaml \\
  --container-runtime=docker \\
  --container-runtime-endpoint=unix:///var/run/dockershim.sock \\
  --dynamic-config-dir=/etc/kubernetes/kubelet \\
  --hostname-override=##NODE_NAME## \\
  --image-pull-progress-deadline=15m \\
  --kubeconfig=/etc/kubernetes/kubelet.kubeconfig \\
  --logtostderr=true \\
  --network-plugin=cni \\
  --pod-infra-container-image=${PAUSE_IMAGE} \\
  --root-dir=${K8S_DIR}/kubelet \\
  --streaming-connection-idle-timeout=4h0m0s \\
  --volume-plugin-dir=${K8S_DIR}/kubelet/kubelet-plugins/volume/exec/ \\
  --v=2
KillMode=process
Restart=on-failure
RestartSec=5
StartLimitInterval=0

[Install]
WantedBy=multi-user.target
EOF

# 生成并分发各节点 kubelet systemd 文件
echo "生成并分发各节点 kubelet systemd 文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_name in ${NODE_NAMES[@]}; do
    echo ">>> ${node_name}"
    sed -e "s/##NODE_NAME##/${node_name}/" kubelet.service.template > kubelet-${node_name}.service
    scp kubelet-${node_name}.service root@${node_name}:/etc/systemd/system/kubelet.service
done

# 创建 kubelet 授权用户
echo "创建 kubelet 授权用户"
if [ $(kubectl get clusterrolebinding kubelet-bootstrap | wc -l) -eq 0 ]; then
    kubectl create clusterrolebinding kubelet-bootstrap --clusterrole=system:node-bootstrapper --group=system:bootstrappers
fi

# 启动 kubelet
echo "启动 kubelet"
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "mkdir -p ${K8S_DIR}/kubelet/kubelet-plugins/volume/exec/"
    ssh root@${node_ip} "/usr/sbin/swapoff -a"
    ssh root@${node_ip} "systemctl daemon-reload && systemctl enable kubelet kube-proxy && systemctl stop kubelet kube-proxy && systemctl restart kubelet kube-proxy"
done

# 检查 kubelet 运行状态
echo "检查 kubelet 运行状态"
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "systemctl status kubelet kube-proxy | grep Active"
done

# 创建自动批准相关 kubelet CSR 请求的 ClusterRole
echo "创建自动批准相关 kubelet CSR 请求的 ClusterRole"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > csr-crb.yaml <<EOF
# Approve all CSRs for the group "system:bootstrappers"
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: auto-approve-csrs-for-group
subjects:
- kind: Group
  name: system:bootstrappers
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: system:certificates.k8s.io:certificatesigningrequests:nodeclient
  apiGroup: rbac.authorization.k8s.io
---
# To let a node of the group "system:nodes" renew its own credentials
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: node-client-cert-renewal
subjects:
- kind: Group
  name: system:nodes
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: system:certificates.k8s.io:certificatesigningrequests:selfnodeclient
  apiGroup: rbac.authorization.k8s.io
---
# A ClusterRole which instructs the CSR approver to approve a node requesting a
# serving cert matching its client cert.
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: approve-node-server-renewal-csr
rules:
- apiGroups: ["certificates.k8s.io"]
  resources: ["certificatesigningrequests/selfnodeserver"]
  verbs: ["create"]
---
# To let a node of the group "system:nodes" renew its own server credentials
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: node-server-cert-renewal
subjects:
- kind: Group
  name: system:nodes
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: approve-node-server-renewal-csr
  apiGroup: rbac.authorization.k8s.io
EOF
kubectl apply -f csr-crb.yaml

# 休息180s在执行手动approve
echo "休息180s在执行手动approve"
sleep 180s

# 手动approve server cert csr
echo "手动approve server cert csr"
source /opt/k8s/bin/env.sh
if [ $(kubectl get csr &> /dev/null | wc -l) -lt 0 ]; then
    /opt/k8s/bin/kubectl get csr | grep "Pending" | awk '{print $1}' | xargs /opt/k8s/bin/kubectl certificate approve
fi

# 验证kubelet
echo "验证kubelet"
source /opt/k8s/bin/env.sh
kubectl create sa kubelet-api
kubectl create clusterrolebinding kubelet-api --clusterrole=system:kubelet-api-admin --serviceaccount=default:kubelet-api
SECRET=$(kubectl get secrets | grep kubelet-api | awk '{print $1}')
TOKEN=$(kubectl describe secret ${SECRET} | grep -E '^token' | awk '{print $2}')
ssh root@${node_ip} "curl -s --cacert /etc/kubernetes/ssl/ca.pem -H "Authorization: Bearer ${TOKEN}" https://${NODE_IP[0]}:10250/metrics | head"
