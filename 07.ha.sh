#!/bin/bash

# 在 master 第一个节点编译安装nginx
echo "在 master 第一个节点编译安装nginx"
source /opt/k8s/bin/env.sh
nginx="nginx-1.16.1"
dir=/usr/local/src
installdir="/opt/k8s/nginx"

interactive=""
if [ -t 1 ];then
    interactive="1"
fi

green() {
    if [ "${interactive}" ];then
        printf '\e[1;31;32m'
    fi
    printf -- "$1"
    if [ "${interactive}" ];then
        printf '\e[0m'
    fi
}

red() {
    if [ "${interactive}" ];then
        printf '\e[1;31;40m'
    fi
    printf -- "$1"
    if [ "${interactive}" ];then
        printf '\e[0m'
    fi
}

download() {
    cd ${dir}
    if [  -f ${dir}/${nginx}.tar.gz ];then
        echo -e "${nginx}.tar.gz file is [$(red "found")]"
    else
       if ! wget --no-check-certificate ${Download_Server}/linux/nginx/${nginx}.tar.gz;then
           echo -e "[$(red "Failed")] download ${nginx}.tar.gz"
           exit 1
       fi
    fi
    tar xf ${nginx}.tar.gz
    if [ $? -eq 0 ];then
        cd ${dir}/${nginx}
    else
        echo -e "Unzip ${nginx}.tar.gz [$(red "failed")]"
    fi
}

compile() {
    download
    cd ${dir}/${nginx}
    ./configure --prefix=/opt/k8s/nginx --conf-path=/opt/k8s/nginx/conf/nginx.conf --with-stream --without-http --without-http_uwsgi_module  --with-threads --without-pcre --without-http-cache --without-http_scgi_module --without-http_fastcgi_module
    make
    make install
}


install_services() {
cat > /etc/systemd/system/nginx.service << EOF
[Unit]
Description=kube-apiserver nginx proxy
After=network.target
After=network-online.target
Wants=network-online.target

[Service]
Type=forking
ExecStartPre=${installdir}/sbin/nginx -c ${installdir}/conf/nginx.conf -p ${installdir} -t
ExecStart=${installdir}/sbin/nginx -c ${installdir}/conf/nginx.conf -p ${installdir}
ExecReload=${installdir}/sbin/nginx -c ${installdir}/conf/nginx.conf -p ${installdir} -s reload
ExecStop=/bin/kill -s QUIT $MAINPID
PrivateTmp=true
Restart=always
RestartSec=5
StartLimitInterval=0
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target
EOF
}

modify_conf() {
cat > ${installdir}/conf/nginx.conf << EOF
worker_processes 2;

pid ${installdir}/logs/nginx.pid;
worker_rlimit_nofile 65535;

events {
    use epoll;
    worker_connections 65535;
}

stream {
    upstream api-server {
        hash \$remote_addr consistent;
        server ${MASTER_IPS[0]}:6443 max_fails=3 fail_timeout=30s;
        server ${MASTER_IPS[1]}:6443 max_fails=3 fail_timeout=30s;
        server ${MASTER_IPS[2]}:6443 max_fails=3 fail_timeout=30s;
    }
    server {
        listen 9443;
        proxy_connect_timeout 1s;
        proxy_pass api-server;
    }
}
EOF
}


[ ! -f ${installdir}/sbin/nginx ] && compile
install_services
modify_conf

# 分发 nginx systemd 到其它节点
echo "分发 nginx systemd 到其它节点"
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    if [ "${node_ip}" != "${MASTER_IPS[0]}"  ]; then
        ssh root@${node_ip} "systemctl daemon-reload && systemctl stop nginx"
        scp /etc/systemd/system/nginx.service root@${node_ip}:/etc/systemd/system/nginx.service
    fi
done

# 复制 nginx 到其它 master 节点
echo "复制 nginx 到其它 master 节点"
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    if [ "${node_ip}" != "${MASTER_IPS[0]}" ]; then
        echo ">>> ${node_ip}"
        # 创建 nginx 目录
        ssh root@${node_ip} "mkdir -p /opt/k8s/nginx"
        scp -pr /opt/k8s/nginx/ root@${node_ip}:/opt/k8s/
    fi
done

# 导入 nginx 环境变量
echo "导入 nginx 环境变量"
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "echo 'export PATH=/opt/k8s/nginx/sbin:$PATH' > /etc/profile.d/nginx.sh"
done

# 安装 keepalived 生成 VIP
echo "安装 keepalived 生成 VIP"
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "yum install -y keepalived"
done

# 生成 keepalived 配置模板文件
echo "生成 keepalived 配置模板文件"
cd  /root/.k8s/deploy/
cat > keepalived.conf-template <<EOF
global_defs {
    notification_email {
        winlu.tec@sunisco.com
    }
    notification_email_from web@sunisco.com
    smtp_server 127.0.0.1
    smtp_connect_timeout 30
    vrrp_skip_check_adv_addr
    vrrp_iptables
    vrrp_garp_interval 0
    vrrp_gna_interval 0
    router_id ##NODE_IP##
}

vrrp_script chk_nginx {
    script "/etc/keepalived/check_port.sh 9443"
    interval 2
    weight -30
}

vrrp_instance VI_1 {
    state MASTER
    interface eth0
    virtual_router_id 251
    priority ##LEVEL##
    advert_int 1

    authentication {
        auth_type PASS
        auth_pass 123456
    }

    unicast_src_ip ##NODE_IP##
    unicast_peer {
        ${MASTER_IPS[0]}
        ${MASTER_IPS[1]}
        ${MASTER_IPS[2]}
    }

    virtual_ipaddress {
        ${LB_IP}/24 dev eth0 label eth0:1
    }

    track_script {
        chk_nginx
    }

    garp_master_delay 1
    garp_master_refresh 5
    notify_master "/etc/keepalived/notify.sh master"
    notify_backup "/etc/keepalived/notify.sh backup"
    notify_fault "/etc/keepalived/notify.sh fault"
}
EOF

# 生成健康检查脚本
echo "生成健康检查脚本"
cd /root/.k8s/deploy

cat > check_port.sh << EOF
#!/bin/bash
CHK_PORT=\$1
if [ -n "\${CHK_PORT}" ]; then
    PORT_PROCESS=\$(ss -lnt | grep \${CHK_PORT} | wc -l)
    if [ \${PORT_PROCESS} -eq 0 ]; then
        echo "Port \${CHK_PORT} Is Not Used,End."
        exit 1
    fi
else
    echo "Check Port Can't Be Empty!"
fi
EOF
chmod +x check_port.sh

cat > notify.sh << EOF
#!/bin/bash
#

vip="${LB_IP}"
contact='it@sunisco.com'

notify() {
    mailsubject="\$(hostname) to be \$1: \$vip floating"
    mailbody="\$(date '+%F %H:%M:%S'): vrrp transition, \$(hostname) changed to be \$1"
    echo \$mailbody | mail -s "\$mailsubject" \$contact
}

case "\$1" in
    master)
        notify master
        exit 0
    ;;
    backup)
        notify backup
        exit 0
    ;;
    fault)
        notify fault
        exit 0
    ;;
    *)
        echo 'Usage: notify.sh {master|backup|fault}'
        exit 1
    ;;
esac
EOF
chmod +x notify.sh

# 生成各节点的 keepalived 配置文件
echo "生成各节点的 keepalived 配置文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
#替换模板文件中的变量，为各节点创建 keepalived 配置文件
for (( i=0; i < 3; i++ )); do
    sed -e "s/##NODE_IP##/${MASTER_IPS[i]}/g" -e "s/##LEVEL##/${LEVEL[i]}/g" keepalived.conf-template > keepalived-${MASTER_IPS[i]}.conf

    if [ "${MASTER_IPS[i]}" == "${MASTER_IPS[0]}"  ]; then
        sed -i '/    '"${MASTER_IPS[0]}"'/d' keepalived-${MASTER_IPS[0]}.conf
    fi

    if [ "${MASTER_IPS[i]}" == "${MASTER_IPS[1]}"  ]; then
        sed -i '/    '"${MASTER_IPS[1]}"'/d' keepalived-${MASTER_IPS[1]}.conf
    fi

    if [ "${MASTER_IPS[i]}" == "${MASTER_IPS[2]}"  ]; then
        sed -i '/    '"${MASTER_IPS[2]}"'/d' keepalived-${MASTER_IPS[2]}.conf
    fi

    if [ "${MASTER_IPS[i]}" == "${MASTER_IPS[1]}" ] || [ "${MASTER_IPS[i]}" == "${MASTER_IPS[2]}"  ]; then
        echo ">>> ${MASTER_IPS[i]} change to BACKUP"
        sed -i 's@MASTER@BACKUP@' keepalived-${MASTER_IPS[i]}.conf
    fi

done

#分发生成的 keepalived 配置文件和检测脚本
echo "分发生成的 keepalived 配置文件和检测脚本"
cd /root/.k8s/deploy
for node_ip in ${MASTER_IPS[@]};do
    echo ">>> ${node_ip}"
    scp keepalived-${node_ip}.conf root@${node_ip}:/etc/keepalived/keepalived.conf
    scp check_port.sh root@${node_ip}:/etc/keepalived/check_port.sh
    scp notify.sh root@${node_ip}:/etc/keepalived/notify.sh
    ssh root@${node_ip} "chmod +x /etc/keepalived/*.sh"
done

# 启动 kube-nginx 和 keepalived
echo "启动 kube-nginx 和 keepalived"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${MASTER_IPS[@]};do
    ssh root@${node_ip} "systemctl daemon-reload && systemctl enable nginx && systemctl restart nginx"
    ssh root@${node_ip} "systemctl daemon-reload && systemctl enable keepalived && systemctl restart keepalived"
done
