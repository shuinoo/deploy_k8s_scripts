!/bin/bash 

# 检查系统发行版
echo "检查系统发行版"
linux_version=`cat /etc/redhat-release`
if [[ ${linux_version} =~ "CentOS" ]];then
    echo -e "\033[32;32m 系统为 ${linux_version} \033[0m \n"
else
    echo -e "\033[32;32m 系统不是CentOS,该脚本只支持CentOS环境\033[0m \n"
    exit 1
fi

# 分发 hosts 到所有主机
echo "分发 hosts 到所有主机"
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp /etc/hosts root@${node_ip}:/etc/hosts
done

# 安装依赖包
echo "安装依赖包"
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "yum install -y conntrack conntrack-tools ntpdate ipvsadm ipset jq iptables curl sysstat libseccomp wget"
done

# 关闭防火墙 Linux 以及 swap 分区
echo "关闭防火墙 Linux 以及 swap 分区"
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "systemctl stop firewalld && systemctl disable firewalld && iptables -F && iptables -X && iptables -F -t nat && iptables -X -t nat"
    ssh root@${node_ip} "swapoff -a && sed -i '/\(.*\)swap/d' /etc/fstab"
done

# 关闭 selinux 
echo "关闭 selinux "
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "setenforce 0 && sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/selinux/config"
done

# 生成加载内核模块文件
echo "生成加载内核模块文件"
cd /root/.k8s/deploy
cat > k8s-modules.conf << EOF
br_netfilter
ip_vs
ip_vs_rr
ip_vs_wrr
ip_vs_sh
nf_conntrack_ipv4
EOF

# 分发生成加载内核模块文件
echo "分发生成加载内核模块文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp k8s-modules.conf root@${node_ip}:/etc/modules-load.d/
    ssh root@${node_ip} "systemctl enable --now systemd-modules-load.service"
done


# 验证模块是否加载成功
echo "验证模块是否加载成功"
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "lsmod | egrep 'ip_vs_rr|br_netfilter|nf_conntrack'"
done

# 优化内核参数
echo "优化内核参数"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > k8s-sysctl.conf << EOF
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-iptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-arptables = 1
net.ipv4.tcp_tw_recycle = 0
net.ipv4.tcp_tw_reuse = 0
vm.swappiness = 0
vm.max_map_count=655360
vm.overcommit_memory=1
vm.panic_on_oom=0
fs.inotify.max_user_watches=1048576
fs.inotify.max_user_instances=8192
fs.nr_open=52706963
fs.file-max=52706963
net.ipv6.conf.all.disable_ipv6=1
net.netfilter.nf_conntrack_max=2310720
EOF

for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp k8s-sysctl.conf root@${node_ip}:/etc/sysctl.d/
    ssh root@${node_ip} "sysctl -p /etc/sysctl.d/k8s-sysctl.conf"
done

# 关闭 sctp
echo "关闭 sctp"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > sctp.conf << EOF
# put sctp into blacklist
install sctp /bin/true
EOF

for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp sctp.conf root@${node_ip}:/etc/modprobe.d/
done

# 设置 ulimit
echo "设置 ulimit"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > k8s-ulimits.conf << EOF
[Manager]
DefaultLimitCORE=infinity
DefaultLimitNOFILE=100000
DefaultLimitNPROC=100000
EOF

for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "mkdir -p /etc/systemd/system.conf.d"
    scp k8s-ulimits.conf root@${node_ip}:/etc/systemd/system.conf.d/
done

# 创建相关目录
echo "创建相关目录"
source /opt/k8s/bin/env.sh
for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    ssh root@${node_ip} "mkdir -p /opt/k8s/bin /etc/{kubernetes,etcd}/ssl"
done

# 导出 k8s 可执行目录
echo "导出 k8s 可执行目录"
source /opt/k8s/bin/env.sh
cat > /root/.k8s/deploy/k8s.sh << EOF
export PATH=/opt/k8s/bin:\$PATH
EOF

for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp /root/.k8s/deploy/k8s.sh root@${node_ip}:/etc/profile.d/k8s.sh
    ssh root@${node_ip} "source /etc/profile.d/k8s.sh"
done


# 安装ceph-common
install_ceph() {
echo "安装ceph-common"
source /opt/k8s/bin/env.sh
cd /root/.k8s/deploy
cat >> ceph.repo << EOF
[Ceph]
name=Ceph packages for \$basearch
baseurl=ftp://ftp-server-usa.joyslink.com/repos/ceph/rpm-nautilus/el7/\$basearch
enabled=1
gpgcheck=1
type=rpm-md
gpgkey=ftp://ftp-server-usa.joyslink.com/repos/ceph/release.asc
priority=1

[Ceph-noarch]
name=Ceph noarch packages
baseurl=ftp://ftp-server-usa.joyslink.com/repos/ceph/rpm-nautilus/el7/noarch
enabled=1
gpgcheck=1
type=rpm-md
gpgkey=ftp://ftp-server-usa.joyslink.com/repos/ceph/release.asc
priority=1

[ceph-source]
name=Ceph source packages
baseurl=ftp://ftp-server-usa.joyslink.com/repos/ceph/rpm-nautilus/el7/SRPMS
enabled=1
gpgcheck=1
type=rpm-md
gpgkey=ftp://ftp-server-usa.joyslink.com/repos/ceph/release.asc
EOF

for node_ip in ${NODE_IPS[@]}; do
    echo ">>> ${node_ip}"
    scp /root/.k8s/deploy/ceph.repo root@${node_ip}:/etc/yum.repos.d/ceph.repo
    ssh root@${node_ip} "yum install -y ceph-common"
done
}
# install_ceph
