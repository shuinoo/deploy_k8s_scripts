#!/bin/bash
node_name="k8s-n-03"

# env.sh
scp -pr /root/k8s/bin/env.sh root@$1:/opt/k8s/bin/env.sh

# 执行系统初始化操作
echo "执行系统初始化操作"
bash /root/.k8s/deploy/scripts/init.sh

# 拷贝 ca 证书到 /etc/kubernetes/ssl
echo "拷贝 ca 证书到 ${node_name}:/etc/kubernetes/ssl"
cd /root/.k8s/deploy
scp -pr ca-key.pem ca.pem root@$1:/etc/kubernetes/ssl/

# 拷贝 kubeconfig 到 /root/.kube/config
echo "拷贝 kubeconfig 到 ${node_name}:/root/.kube/config"
cd /root/.k8s/deploy
ssh root@$1 "mkdir -p /root/.kube/"
scp -pr kubectl.kubeconfig root@$1:/root/.kube/config

# 拷贝 kubeadm kubectl kubelet kube-proxy 到 /opt/k8s/bin/
echo "拷贝 kubeadm kubectl kubelet kube-proxy 到 ${node_name}:/opt/k8s/bin/"
cd /root/.k8s/deploy/kubernetes
scp -pr node/bin/{kubeadm,kubectl,kubelet,kube-proxy} root@$1:/opt/k8s/bin/

# 安装 docker 所需的包
echo "${node_name} 安装 docker 所需的包"
ssh root@$1 "yum install -y yum-utils device-mapper-persistent-data lvm2"

# 安装 docker
echo "安装 docker" 
cd /root/.k8s/deploy
source /root/.k8s/env.sh
ssh root@$1 "yum install docker-ce-${DOCER_VERSION}"

# 拷贝 docker daemon.json 到 /etc/docker
echo "拷贝 docker daemon.json 到 ${node_name}:/etc/docker"
cd /root/.k8s/deploy
source /root/.k8s/env.sh
ssh root@$1 "mkdir -p /etc/docker"
scp -pr daemon.json root@$1:/etc/docker/

# 启动 docker
echo "启动 ${node_name} docker"
ssh root@$1 "systemctl daemon-reload && systemctl stop docker && systemctl restart docker"

# 拷贝 cni 二进制文件到 /opt/k8s/bin/
echo "拷贝 cni 二进制文件到 ${node_name}:/opt/k8s/bin/"
cd /root/.k8s/deploy
scp -pr cni/{bridge,host-local,loopback} root@$1:/opt/k8s/bin

# 拷贝 cni 默认配置文件到 /etc/cni/net.d
echo "拷贝 cni 默认配置文件到 ${node_name}:/etc/cni/net.d"
cd /root/.k8s/deploy
ssh root@$1 "mkdir -p /etc/cni/net.d"
scp -pr cni/cni-default.conf root@$1:/etc/cni/net.d/

# 生成 kubelet bootstrap kubeconfig 文件
echo "生成 ${node_name} kubelet bootstrap kubeconfig 文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
# 创建 token
export BOOTSTRAP_TOKEN=$(kubeadm token create \
  --description kubelet-bootstrap-token \
  --groups system:bootstrappers:${node_name} \
  --kubeconfig ~/.kube/config)
# 设置集群参数
kubectl config set-cluster kubernetes \
  --certificate-authority=/etc/kubernetes/ssl/ca.pem \
  --embed-certs=true \
  --server=${KUBE_APISERVER} \
  --kubeconfig=kubelet-bootstrap-${node_name}.kubeconfig
# 设置客户端认证参数
kubectl config set-credentials kubelet-bootstrap \
  --token=${BOOTSTRAP_TOKEN} \
  --kubeconfig=kubelet-bootstrap-${node_name}.kubeconfig
# 设置上下文参数
kubectl config set-context default \
  --cluster=kubernetes \
  --user=kubelet-bootstrap \
  --kubeconfig=kubelet-bootstrap-${node_name}.kubeconfig
# 设置默认上下文
kubectl config use-context default --kubeconfig=kubelet-bootstrap-${node_name}.kubeconfig

# 查看 kubeadm 创建的 token
echo "查看 kubeadm 创建的 ${node_name} token"
source /opt/k8s/bin/env.sh
kubeadm token list --kubeconfig ~/.kube/config

# 查看 token 关联的 Secret
echo "查看 ${node_name} token 关联的 Secret"
source /opt/k8s/bin/env.sh
kubectl get secrets -n kube-system | grep bootstrap-token

# 分发该节点 kubelet bootstrap kubeconfig 文件
echo "分发 ${node_name} kubelet bootstrap kubeconfig 文件"
cd /root/.k8s/deploy
scp kubelet-bootstrap-${node_name}.kubeconfig root@$1:/etc/kubernetes/kubelet-bootstrap.kubeconfig

# 创建该节点 kubelet 配置文件
echo "创建 ${node_name} kubelet 配置文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > kubelet-config-${1}.yaml.template << EOF
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
address: "$1"
authentication:
  anonymous:
    enabled: false
  webhook:
    cacheTTL: 2m0s
    enabled: true
  x509:
    clientCAFile: "/etc/kubernetes/ssl/ca.pem"
authorization:
  mode: Webhook
  webhook:
    cacheAuthorizedTTL: 5m0s
    cacheUnauthorizedTTL: 30s
cgroupDriver: cgroupfs
cgroupsPerQOS: true
clusterDNS:
  - "${CLUSTER_DNS_SVC_IP}"
clusterDomain: "${CLUSTER_DNS_DOMAIN}"
configMapAndSecretChangeDetectionStrategy: Watch
containerLogMaxSize: 20Mi
containerLogMaxFiles: 10
enforceNodeAllocatable: ["pods"]
eventBurst: 20
eventRecordQPS: 5
evictionHard:
  memory.available: "100Mi"
  nodefs.available: "10%"
  nodefs.inodesFree: "5%"
  imagefs.available: "15%"
evictionPressureTransitionPeriod: 5m0s
failSwapOn: true
featureGates:
  RotateKubeletServerCertificate: true
  RotateKubeletClientCertificate: true
fileCheckFrequency: 20s
hairpinMode: hairpin-veth
healthzBindAddress: "$1"
healthzPort: 10248
httpCheckFrequency: 20s
kubeReserved: {}
kubeAPIBurst: 2000
kubeAPIQPS: 1000
maxPods: 220
podCIDR: "${CLUSTER_CIDR}"
port: 10250
readOnlyPort: 0
resolvConf: /etc/resolv.conf
rotateCertificates: true
serializeImagePulls: true
serverTLSBootstrap: true
EOF

# 分发该节点 kubelet 配置文件
echo "分发${node_name} kubelet 配置文件"
cd /root/.k8s/deploy
scp kubelet-config-$1.yaml.template root@$1:/etc/kubernetes/kubelet-config.yaml

# 生成该各节点 kubelet systemd 文件
echo "生成分发 ${node_name} kubelet systemd 文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > kubelet-${node_name}.service <<EOF
[Unit]
Description=Kubernetes Kubelet
Documentation=https://github.com/GoogleCloudPlatform/kubernetes
After=docker.service
Requires=docker.service

[Service]
WorkingDirectory=${K8S_DIR}/kubelet
ExecStart=/opt/k8s/bin/kubelet \\
  --bootstrap-kubeconfig=/etc/kubernetes/kubelet-bootstrap.kubeconfig \\
  --cert-dir=/etc/kubernetes/ssl \\
  --cni-bin-dir=/opt/k8s/bin \\
  --cni-conf-dir=/etc/cni/net.d \\
  --config=/etc/kubernetes/kubelet-config.yaml \\
  --container-runtime=docker \\
  --container-runtime-endpoint=unix:///var/run/dockershim.sock \\
  --hostname-override=${node_name} \\
  --image-pull-progress-deadline=15m \\
  --kubeconfig=/etc/kubernetes/kubelet.kubeconfig \\
  --logtostderr=true \\
  --network-plugin=cni \\
  --pod-infra-container-image=${POD_IMAGE} \\
  --root-dir=${K8S_DIR}/kubelet \\
  --streaming-connection-idle-timeout=4h0m0s \\
  --volume-plugin-dir=${K8S_DIR}/kubelet/kubelet-plugins/volume/exec/ \\
  --v=2
KillMode=process
Restart=on-failure
RestartSec=5
StartLimitInterval=0

[Install]
WantedBy=multi-user.target
EOF

# 分发该节点 kubelet systemd 文件
echo "分发 ${node_name} kubelet systemd 文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
ssh root@$1 "mkdir -p ${K8S_DIR}/kubelet/kubelet-plugins/volume/exec/"
scp kubelet-${node_name}.service root@${node_name}:/etc/systemd/system/kubelet.service

# 启动 kubelet
echo "启动 ${node_name} kubelet"
ssh root@$1 "/usr/sbin/swapoff -a"
ssh root@$1 "systemctl daemon-reload && systemctl enable kubelet kube-proxy && systemctl stop kubelet kube-proxy && systemctl restart kubelet kube-proxy"

# 检查 kubelet 运行状态
echo "检查 kubelet 运行状态"
source /opt/k8s/bin/env.sh
ssh root@$1 "systemctl status kubelet kube-proxy | grep Active"

# 查看 kubelet 证书签名请求
echo "查看 kubelet 证书签名请求"
kubectl get csr

# 休息180s在执行手动approve
echo "休息180s在执行手动approve"
sleep 180s

# 手动approve server cert csr
echo "手动approve server cert csr"
source /opt/k8s/bin/env.sh
if [ $(kubectl get csr &> /dev/null | wc -l) -lt 0 ]; then
    /opt/k8s/bin/kubectl get csr | grep "Pending" | awk '{print $1}' | xargs /opt/k8s/bin/kubectl certificate approve
fi

# 分发 calico 证书和私钥到该节点
echo "分发 ${node_name} calico 证书和私钥到该节点"
cd /root/.k8s/deploy
ssh root@$1 "mkdir -p /etc/calico/ssl"
scp calico-key.pem calico.pem root@$1:/etc/calico/ssl/

# 删除原有 cni 配置
echo "删除 ${node_name} 原有 cni 配置"
ssh root@$1 "mv /etc/cni/net.d/cni-default.conf /root/"

# 下载 calicoctl 客户端
echo "${node_name} 下载 calicoctl 客户端"
cd /root/.k8s/deploy
ssh root@$1 "cd /opt/k8s/bin && wget ${Download_Server}/linux/${CALICO_VERSION}/calicoctl-linux-amd64 && mv calicoctl-linux-amd64 calicoctl && chmod +x calicoctl"

# 分发 calicoctl 配置文件
echo "分发 calicoctl 配置文件到 ${node_name}"
cd /root/.k8s/deploy
scp calicoctl.cfg root@$1:/etc/calico/

# 分发 kube-proxy kubeconfig 到该节点 
echo "分发 kube-proxy kubeconfig 到 ${node_name}"
cd /root/.k8s/deploy
scp kube-proxy.kubeconfig root@$1:/etc/kubernetes/

# 创建该节点 kube-proxy 配置文件
echo "创建 ${node_name} kube-proxy 配置文件"
cd /root/.k8s/deploy
source /opt/k8s/bin/env.sh
cat > kube-proxy-config-${node_name}.yaml.template << EOF
kind: KubeProxyConfiguration
apiVersion: kubeproxy.config.k8s.io/v1alpha1
clientConnection:
  burst: 200
  kubeconfig: "/etc/kubernetes/kube-proxy.kubeconfig"
  qps: 100
bindAddress: $1
healthzBindAddress: $1:10256
metricsBindAddress: $1:10249
enableProfiling: true
clusterCIDR: ${CLUSTER_CIDR}
hostnameOverride: ${node_name}
mode: "ipvs"
portRange: ""
kubeProxyIPTablesConfiguration:
  masqueradeAll: false
kubeProxyIPVSConfiguration:
  scheduler: rr
  excludeCIDRs: []
EOF

# 分发该节点的 kube-proxy 配置文件
echo "分发 ${node_name} 的 kube-proxy 配置文件"
cd /root/.k8s/deploy
scp kube-proxy-config-${node_name}.yaml.template root@$1:/etc/kubernetes/kube-proxy-config.yaml

# 分发 kube-proxy systemd 文件
echo "分发 ${node_name} kube-proxy systemd 文件"
cd /root/.k8s/deploy
scp kube-proxy.service root@$1:/etc/systemd/system/

# 启动 kube-proxy
echo "启动 ${node_name} kube-proxy"
cd /root/.k8s/deploy
ssh root@$1 "mkdir -p ${K8S_DIR}/kube-proxy "
ssh root@$1 "systemctl daemon-reload && systemctl enable kube-proxy && systemctl stop kube-proxy && systemctl restart kube-proxy"

# 设置该节点的角色
echo "设置 ${node_name} 的角色"
until kubectl get nodes | grep "${node_name}"; do
    kubectl label node ${node_name} kubernetes.io/role=node --overwrite
done
