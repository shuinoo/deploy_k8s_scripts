#!/bin/bash 

# 设置 master 节点的 role 标签
echo "设置 master 节点的 role 标签"
source /opt/k8s/bin/env.sh
for master_name in ${MASTER_NAMES[@]}; do
    echo ">>> ${master_name}"
    kubectl label node ${master_name} kubernetes.io/role=master --overwrite
done

# 设置 node 节点的 role 标签
echo "设置 node 节点的 role 标签"
source /opt/k8s/bin/env.sh
for work_name in ${WORK_NAMES[@]}; do
    echo ">>> ${work_name}"
    kubectl label node ${work_name} kubernetes.io/role=node --overwrite
done

# 设置 master 节点污点
echo "设置 master 节点污点"
source /opt/k8s/bin/env.sh
for master_name in ${MASTER_NAMES[@]}; do
    echo ">>> ${master_name}"
    kubectl cordon ${master_name}
done
