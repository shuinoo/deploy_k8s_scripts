#!/bin/bash 

# 下载 helm 二进制文件
echo "下载 helm 二进制文件"
cd /root/.k8s
source /opt/k8s/bin/env.sh
wget ${Download_Server}/helm/helm-v3.4.2-linux-amd64.tar.gz
tar xf helm-v3.4.2-linux-amd64.tar.gz
rm -f helm-v3.4.2-linux-amd64.tar.gz
mv linux-amd64 helm

# 移动 helm 二进制文件到 /opt/k8s/bin
echo "移动 helm 二进制文件到 /opt/k8s/bin"
source /opt/k8s/bin/env.sh
cd /root/.k8s
cp helm/helm /opt/k8s/bin/

# 添加 helm repo 源
helm repo add stable          https://charts.helm.sh/stable
helm repo add incubator       https://charts.helm.sh/incubator
helm repo add ingress-nginx   https://kubernetes.github.io/ingress-nginx
helm repo add jetstack        https://charts.jetstack.io
helm repo add traefik         https://helm.traefik.io/traefik
helm repo add weiphone        http://registry.joyslink.com/chartrepo/weiphone
